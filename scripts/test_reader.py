 #!/usr/bin/env python2
import getopt
import sys
import os
import string
import grAccess
import itgadate
import csv
import configuration
import sybutils


config   = configuration.Configuration()
dbname   = '%s' % (config['database']['quant'])
host     = '%s' % (config['database']['server'])
wuser    = '%s' % (config['database']['wuser'])
wpasswd  = '%s' % (config['database']['wpassword'])

buyBacksMap = {}

buyBackFile = '/data/aftertax/warakirri/buyBackFile'

def openPrice(instr, date):
    cmd = 'histrep -ano -n -C -Fx -d %s %s | cut -d, -f2' % (date, instr)
    fd = os.popen(cmd)
    return float(fd.readline()[:-1].strip())

def GetTotalShares(index, instr, date):
    db = sybutils.connect(host, wuser, wpasswd, dbname)
    c=db.cursor()

    date = itgadate.date_before_weekdays(date, 1)
    cmd = 'select sum(totalissue) from %s..atb_tax_lots where index_id = %s and instr_id = %s and %s between start_date and end_date' % (dbname, index, instr, date)

    try:
        c.execute(cmd)
    except sybutils.Sybase.Error, message:
        print 'failed to perform command: %s' % (cmd)
        print message
        sys.exit(1)

    for row in c.fetchall():
        return float(row[0])

    sys.stderr.write('ERROR: Could not determine shares for buy back index %s instrument %s date %s\n' % (index_id, instr_id, date))
    return 0

FD = open(buyBackFile, 'r')
index = 12001555
for line in FD:
    if (len(line) == 0):
        continue
    if (line[0] == '#'):
        continue
    try:
        [date,index_id,instr_id,cgt_cash,received_cash,dividend,franked,percentage] = string.split(line,',',8)
        if (int(index_id) == index):
            if (not buyBacksMap.has_key(date)):
                buyBacksMap[date] = 0
            sharesSold         = float(percentage) * float(GetTotalShares(index_id, instr_id, date))
            buyBackPrice       = float(received_cash) + float(dividend)
            repurchasePrice    = float(openPrice(instr_id, date))
            buyBacksMap[date] += sharesSold * (repurchasePrice - buyBackPrice)
    except:
        sys.stderr.write('ERROR: cannot read line [%s] from buy back file\n' % line)
        sys.exit(1)