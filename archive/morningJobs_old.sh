#!/bin/sh
#
# $Header: /import/cvs/client/bin/morningJobs.sh,v 1.227 2020/04/23 06:00:10 qdsbuild Exp $
#----------------------------------------------------------------------------
#  SYNOPSIS
#	morningJobs.sh
#----------------------------------------------------------------------------
#  DESCRIPTION
#	Runs the list of jobs which are to be performed after the overnight
#	processing has been completed. 
#----------------------------------------------------------------------------
PATH=/usr/bin:/usr/local/bin:/usr/sfw/bin:/projects/qds/bin:/projects/client/bin:/projects/aftertax/bin
export PATH
PERLLIB=$PERLLIB:/projects/perl5
export PERLLIB
. /opt/sybase_15_0_3_OC/SYBASE.sh

# Source QDS common script first
common_dir=`read_config.py -s common:dir`
. $common_dir/utility.sh

today=`dateformat -i`
prevDay=`dateformat -P $today`
sendMail=1
republish=0
ownerMail=`read_config.py -s client:alertmail`
noSendArg=""
non_trading_file=`read_config.py -s nontradingfile`
baseDir=`read_config.py -s client:data_dir`
logDir=`read_config.py -s client:log_dir`
logDir=$logDir/morning
echo log directory is $logDir

# log file to capture timing info
timeLog=$logDir/timestamps.log
rm -f $timeLog
timestamp () {
	stamp=`date '+%H:%M:%S'`
	echo "$@: $stamp" >> $timeLog
}

Usage="Usage: $0 [-d date] [-n] [-m address] [-R]
-d date: Defaults to current date 
-n: Don't send data emails to clients (internal status mail are still sent)
-m address: Internal status mail address (default to $ownerMail)
-R : republish
"

while getopts "nd:m:R" c ; do
	case $c in
		n) sendMail=0;;
		m) ownerMail=$OPTARG;;
		d) today=$OPTARG;;
		R) republish=1;;
		*) echo "$Usage"; 
		exit 1;;
	esac
done

if [ $sendMail -eq 0 ]; then
	noSendArg="-n"
fi

# check that overnight processing has finished

# note: if Monday, date on file may be from Saturday or Sunday.  If that is so,
# then the file will already exist since the run was done on the weekend, and
# this is called on Monday. 

today=`dateformat -i $today`
echo today is $today
prevDay=`dateformat -P $today`
prevWeek=`dateformat -W -1 $today`
Dow=`dateformat -c $today | awk '{print $1};'`

if [ `dateformat -p $today` != `dateformat -P $today` ]; then
	isMonday="TRUE"
else
	isMonday="FALSE"
fi

flagfile_dir="`read_config.py -s dbaseload_dir`/data/flagfiles"
satFile=${flagfile_dir}/`dateformat -p -p -i $today`.7am
sunFile=${flagfile_dir}/`dateformat -p -i $today`.7am
todayFile=${flagfile_dir}/`dateformat -i $today`.7am
echo todayFile is $todayFile
echo isMonday is $isMonday
echo satFile is $satFile
echo sunFile is $sunFile

if [ "$isMonday" != "TRUE"  -o \( ! -r $satFile -a ! -r $sunFile \) ];  then
	# check to see if overnight processing has been done for today
	echo "waiting for overnight processing"
	if [ `dateformat -i` -eq $today ] ; then
		while [ ! -r $todayFile -a `date +%H%M` -lt 0900 ]
		do
			echo "Sleeping"
			sleep 300
		done
	fi

	if [ ! -r $todayFile -a `date +%H%M` -ge 0900 ] ; then
		echo "**ERROR** Waited until 9:00 for ${flagfile_dir}/${today}.7am to appear!!" | tee $logDir/morningJobs.log | mailx -s "HELP! client morning jobs" $ownerMail
		exit 1
	fi
fi

# (3) CFS
# No longer required as per Wang Fei from Colonial state
mailTarget=$ownerMail
#echo "Custom indices for Colonial First State - STORM and Ex-CBA"
#if [ $sendMail -eq 1 ]; then
#	mailTarget="CFSFundAccounting@cba.com.au,$ownerMail"
#fi
#histrep -C -d `dateformat -P $today` -FX B-JXCB B-JXCB2 B-100MP B-100MP2 B-KIMP B-KIMP2 B-KMPT B-KMPT2 1> /tmp/CFS_Custom.csv 2> $logDir/CFS_Custom.log
#mail.py -s "XAO ex CBA, XTO ex Prop, 300 ex Prop" -t $mailTarget /tmp/CFS_Custom.csv

# (4) CFS
# No Longer Required as per Elly Grace
mailTarget=$ownerMail
#echo "300I ex Property"
#if [ $sendMail -eq 1 ]; then
#	mailTarget="Research&PerformanceInformation@colonialfirststate.com.au,mtran@colonialfirststate.com.au,aly@colonialfirststate.com.au,$ownerMail"
#fi
#histrep -d `dateformat -P $today` -F X B-KIMP B-KIMP2 B-KMPT B-KMPT2 1> /tmp/CFS_KIMP_KMPT.csv 2> $logDir/CFS_KIMP_KMPT.log
#mail.py -s "300I less XPT" -t $mailTarget /tmp/CFS_KIMP_KMPT.csv

# (5) GICS for perpetual for rothschild/perpetual investments
#
echo "GICS for perpetual for rothschild/perpetual investments"
for mailPeople in rbcdexiaperformance@rbcdexia.com austperformance@jpmorgan.com; do 
    perpDoDaily300Gics.sh $noSendArg -d `dateformat -P $today` -m $mailPeople 2>&1 | tee $logDir/perpDoDaily300Gics.log | mailx -s "Perpetual/Rothschild GICS 4pm daily (perpDoDaily300Gics.sh)" $ownerMail
done

# (6) Perpetual - 300 GICS SECTOR Changes
#
echo Perpetual - 300 GICS SECTOR Changes
timestamp "perpIndexChanges.py START"
perpIndexChanges.py $noSendArg -d $prevDay 2>&1 | tee $logDir/perpIndexChanges.log | mailx -s "Perpetual 300 GICS Changes" $ownerMail
timestamp "perpIndexChanges.py END"

# (8) BT daily after tax files
#
echo "BT daily after tax files"
( btAfterTaxData.sh $noSendArg 2>&1 ) | tee $logDir/btAfterTaxData.log | mailx -s "BT - daily after tax files" $ownerMail

# (10) CBUS Orbis daily after tax files
#
echo "CBUS Orbis daily after tax files"
( cbusAfterTaxDataReinvested.sh -D $noSendArg 2>&1 ) | tee $logDir/cbusAfterTaxDataReinvested.log | mailx -s "CBUS Orbis daily after tax files" $ownerMail

# (11) CFS - send accumulation indices (currently only in morning, but will change)

echo CFS accumulation
timestamp "cfsdailyGICS.sh START"
cfsdailyGICS.sh $noSendArg -u -a -d $prevDay 2>&1 | tee $logDir/cfsdailyGICS.log | mailx -s "CFS" $ownerMail
timestamp "cfsdailyGICS.sh END"

# (12) Mercury GICS attribution files
#
timestamp "mamDoDaily300Gics.sh START"
mamDoDaily300Gics.sh $noSendArg -d $prevDay 2>&1 | tee $logDir/mamDoDaily300Gics.log | mailx -s "Mercury GICS daily (mamDoDaily300Gics.sh)" $ownerMail
timestamp "mamDoDaily300Gics.sh END"

# (13) Colonial First State - daily data for the stocks in XAO and gicsgroup info 
# FirstState GICS attribution files
#
echo Colonial First State - daily attribution data 
timestamp "fsDoDailyGICS.log START"
fsDoDailyGICS.sh $noSendArg -d $prevDay 2>&1 | tee $logDir/fsDoDailyGICS.log | mailx -s "First State data done (GICS)" $ownerMail
timestamp "fsDoDailyGICS.log END"

# (14) BT - 150 ex 50 index changes check
#
echo "BT - 150 ex 50 index changes check"        
bt_send_alerts_sde.py -d $prevDay 2>&1 | tee $logDir/bt_send_alerts_sde.log | mailx -s "BT - 150 ex 50 index changes check" $ownerMail
timestamp "bt_send_alerts_sde.py END"

# (15) BT - Backoffice Data (FMC + Extra) NO PROP SPLIT
#
echo "BT - Backoffice Data NO PROP SPLIT"        
timestamp "btBackOfficeLoad.sh NO PROP SPLIT START"
btBackOfficeLoad.sh $noSendArg -u -h -d $prevDay 2>&1 | tee $logDir/btBackOfficeLoad_daily_no_prop_split.log | mailx -s "BT - 9am Backoffice Data (no prop split)" $ownerMail

if [ $Dow = "Tue" ]; then
	# Sundays Data
	btBackOfficeLoad.sh $noSendArg -u -h -d `dateformat -p $prevDay` 2>&1 | tee $logDir/btBackOfficeLoad_sunday_no_prop_split.log | mailx -s "BT - 9am Backoffice Data (no prop split)" $ownerMail

	# Saturdays Data
	btBackOfficeLoad.sh $noSendArg -u -h -d `dateformat -pp $prevDay` 2>&1 | tee $logDir/btBackOfficeLoad_saturday_no_prop_split.log | mailx -s "BT - 9am Backoffice Data (no prop split)" $ownerMail
fi
timestamp "btBackOfficeLoad.sh NO PROP SPLIT END"

# (16) BT NO PROP SPLIT
#
echo "BT - Preliminary Custom Index Data NO PROP SPLIT"        
timestamp "btPrelimIndexData.sh START NO PROP SPLIT"
btPrelimIndexData.sh $noSendArg -d $today 2>&1 | tee $logDir/btPrelimIndexData_no_prop_split.log | mailx -s "BT - Preliminary Custom Index data (no prop split)" $ownerMail
timestamp "btPrelimIndexData.sh END NO PROP SPLIT"

# (17) CFS
#
echo "CFS Market Data"
timestamp "cfsData.sh START"
( cfsData.sh $noSendArg -c -d $prevDay 2>&1 | tee $logDir/cfsData.log | mailx -s "CFS -- Market Data" $ownerMail )
timestamp "cfsData.sh END"

# (19) Blackrock - index values
#
echo Blackrock - index values
timestamp "blackrockIndexValues.sh START"
( blackrockIndexValues.sh $noSendArg -b `dateformat -PPP $prevDay` -e $prevDay 2>&1 ) | tee $logDir/blackrockIndexValues.log | mailx -s "Blackrock Index Values - Daily" $ownerMail
timestamp "blackrockIndexValues.sh END"

# (20) Wilson HTM daily files
#
echo "Wilson HTM daily files"
( wilsonWeightsReturnsDaily.sh $noSendArg -d $prevDay 2>&1 ) | tee $logDir/wilsonWeightsReturnsDaily.log | mailx -s "Wilson HTM daily files" $ownerMail

# (23) Daily Warakirri FMC
#
echo "Daily Warakirri Upload"
timestamp "warakirriFMCFiles.sh START"
( warakirriFMCFiles.sh $noSendArg -d $today 2>&1 ) | tee $logDir/warakirriFMCFiles.log | mailx -s "Daily Warakirri Upload" $ownerMail
timestamp "warakirriFMCFiles.sh END"

# (24) Warakirri index data
#
echo "After-tax ginraw data for Warakirri"
timestamp "warakirriIndexData.sh START"
( warakirriIndexData.sh $noSendArg -d $today 2>&1 ) | tee $logDir/warakirriIndexData.log | mailx -s "After-tax ginraw data for Warakirri" $ownerMail
timestamp "warakirriIndexData.sh END"

# (26) Continuum Capital
#
echo "Continuum Capital Market Data"
timestamp "ccData.sh START"
( ccData_post_mis_decommission.sh $noSendArg -d $prevDay 2>&1 ) | tee $logDir/ccData.log | mailx -s "Continuum Capital Market Data" $ownerMail
timestamp "ccData.sh END"

# (27) BNP Paribas
#
echo "BNP Paribas Franked Index Data"
timestamp "bnpDailyFrankedData.sh START"
( bnpDailyFrankedData.sh $noSendArg -d $prevDay 2>&1 ) | tee $logDir/bnpDailyFrankedData.log | mailx -s "BNP Paribas Franked Indices" $ownerMail
timestamp "bnpDailyFrankedData.sh END"

# (28) Challenger
#
echo "Challenger franking percentage data for I-200"
( challengerFrankPercent.sh $noSendArg -i I-200 -c XJO -b $prevDay -e $prevDay 2>&1 ) | tee $logDir/challengerFrankPercent.log | mailx -s "Challenger franking percentage data for I-200" $ownerMail

# (29) Longreach
#
echo "Longreach index value data for I-300"
( catholicAfterTaxData.sh $noSendArg -D 1 -b $prevDay -e $prevDay 2>&1 ) | tee $logDir/longreachindexvalue.log | mailx -s "Longreach index value data for I-300" $ownerMail


########## After tax data and flags - Begin ###############

# after tax benchmark - Ratio file check
echo "After tax benchmark - Ratio file check"
timestamp "atbCheckRatios.sh START"
atbCheckRatios.sh
timestamp "atbCheckRatios.sh END"

# after tax benchmark - data flags
echo "After tax benchmark - data flag"
timestamp "ATB data flag - START"
fn=`read_config.py -s aftertax:data_dir`/flagfiles/${prevDay}.Data.Confirm
touch $fn
if [ $? -ne 0 ]; then
    echo ERROR: Could not touch flag file ${fn} | tee $logDir/ATBDataFlag.log | mailx -s "ATB - Data flag" $ownerMail
else
    echo Set flag: ${fn} | tee $logDir/ATBDataFlag.log | mailx -s "ATB - Data flag" $ownerMail
fi
timestamp "ATB data flag - END"

########## After tax data and flags - End ###############


# following line should be the last line of this script
# because it checks for potential errors in the output
# produced by all the jobs
egrep -i "wrong|out of order|not|err|fail|ille|unab|segm|warn|trace|core|denied|get_old_prices|timed out|time out|abort|deadlock|Duplicate entries in|can't|don't|must be > 0|test" $logDir/* | egrep -iv 'deprecat|FileChecksAndReformat.log|no change|no dividend|notification.py|_latest|N do not mail to clients|no data to send|Cannot get \[Cashflows|DailyReports.log' > /tmp/errors.morningJobs
if [ `wc -l /tmp/errors.morningJobs | awk '{print $1}'` -ne 0 ]; then
    warn "Potential errors exist in morning jobs"
    cat /tmp/errors.morningJobs | mailx -s "morningJobs: potential errors from log directory" $ownerMail
fi

#########################################
########## Solaris Attribution ##########
#########################################
echo "Solaris Wealth - Attribution"
timestamp "Solaris Wealth - Attribution - START"
( LOGNAME=swealth; export LOGNAME; /projects/attribution/swealth/bin/FileChecksAndReformat.sh -w -d ${prevDay} 2>&1 ) | tee $logDir/FileChecksAndReformat.log | mailx -s "Solaris Wealth - Attribution - FileChecksAndReformat" $ownerMail
if [ `egrep 'WARN|ERROR' $logDir/FileChecksAndReformat.log | egrep -v "WARNING: There is no transactions" | wc -l ` -ne 0 ]; then
    warn "Solaris Wealth attribution load error"
else
    ( LOGNAME=swealth; export LOGNAME; /projects/attribution/swealth/bin/DailyReports.sh -d ${prevDay} 2>&1 ) | tee $logDir/DailyReports.log | mailx -s "Solaris Wealth - Attribution - DailyReports" $ownerMail
    timestamp "Solaris Wealth - Attribution - END"
fi
