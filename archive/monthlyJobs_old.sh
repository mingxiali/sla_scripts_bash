#!/bin/sh
#----------------------------------------------------------------------------
#  SYNOPSIS
#	monthlyJobs.sh
#----------------------------------------------------------------------------
#  DESCRIPTION
#	Runs the list of jobs which are to be performed on the first of the month 
#	after the overnight processing has been completed. 
#----------------------------------------------------------------------------
# $Header: /import/cvs/client/bin/monthlyJobs.sh,v 1.92 2021/01/04 02:42:12 qdsbuild Exp $
#----------------------------------------------------------------------------

PATH=/usr/bin:/usr/local/bin:/usr/sfw/bin:/projects/qds/bin:/projects/client/bin:/projects/aftertax/bin
export PATH
PERLLIB=$PERLLIB:/projects/perl5
export PERLLIB
. /opt/sybase_15_0_3_OC/SYBASE.sh

# Source QDS common script first
common_dir=`read_config.py -s common:dir`
. $common_dir/utility.sh

today=`dateformat -i`
sendMail=1
ownerMail=`read_config.py -s client:alertmail`
dataDir=`read_config.py -s client:data_dir`
logDir=`read_config.py -s client:log_dir`
logDir=$logDir/monthly
echo log directory is $logDir
echo data directory is $dataDir

Usage="Usage: $0 [-d date] [-n] [-m address]
    -d date: Defaults to current date 
    -n: Don't send data emails to clients (internal status mail are still sent)
	-m address: Internal status mail address (default to $ownerMail)
"

while getopts "nd:m:" c ; do
    case $c in
    n) sendMail=0;;
	m) ownerMail=$OPTARG;;
	d) today=$OPTARG;;
    *) echo "$Usage";
        exit 1;;
    esac
done

noSendArg=""

if [ $sendMail -eq 0 ]; then
    noSendArg="-n"
fi

# check that overnight processing has finished
# note: if Monday, or weekend, date on file may be from Saturday or Sunday.  

echo today is $today
prevDay=`dateformat -P $today`
prevMonth=`dateformat -M -1 $today`
prevMonthBegin=`echo $prevMonth | cut -c1-6`01


set `dateformat -c $today`
day=$1

if [ "$day" = "Sat" -o "$day" = "Sun" -o "$day" = "Mon" ]; then
	isWeekend="TRUE"
else
	isWeekend="FALSE"
fi

flagfile_dir="`read_config.py -s dbaseload_dir`/data/flagfiles"
prev2DayFile=${flagfile_dir}/`dateformat -p -p -i $today`.7am
prevDayFile=${flagfile_dir}/`dateformat -p -i $today`.7am
todayFile=${flagfile_dir}/`dateformat -i $today`.7am
echo todayFile is $todayFile
echo prevDayFile is $prevDayFile
echo prev2DayFile is $prev2DayFile

if [ "$day" != "Mon"  -o \( ! -r $prev2DayFile -a ! -r $prevDayFile \) ];  then
	if [ "$day" != "Sun"  -o \( ! -r $prevDayFile \) ];  then
		# check to see if overnight processing has been done for today
		echo "waiting for overnight processing"
		if [ `dateformat -i` -eq $today ] ; then
			while [ ! -r $todayFile -a `date +%H%M` -lt 1700 ]
			do
				echo "Sleeping"
        		sleep 300
			done
		fi

		if [ ! -r $todayFile -a `date +%H%M` -ge 1700 ] ; then
        	echo "**ERROR** Waited until 5:00pm for ${flagfile_dir}/${today}.7am to appear!!" | tee $logDir/monthlyJobs.log
        	echo "**ERROR** Waited until 5:00pm for ${flagfile_dir}/${today}.7am to appear!!  Need to rerun the monthly client jobs when the processing is complete" | tee -a $logDir/monthlyJobs.log | mailx -s "HELP! client monthly jobs" $ownerMail
        	exit 1
		fi
	fi
fi

echo "OK - ready to go"

beginDate=`dateformat -M -1 $today`
beginDate=`dateformat -P $beginDate`
beginDate=`dateformat -N $beginDate`

mailTarget=$ownerMail
echo "300 franked index to RBC"
if [ $sendMail -eq 1 ]; then
    mailTarget=rbcausperformance@rbc.com,dxaup@rbc.com,gts.ap.mo.performance@imcap.ap.ssmb.com,Brian.OMarnain@bennelongfunds.com,aravinda.jayaram@citi.com
fi
histrep -C -n -b $beginDate -e $prevDay -FX B-300F2 | tee $logDir/Bennelong-300F2.log > /tmp/300F.csv
mail.py -s "300 Franked Index" -t $ownerMail -b $mailTarget /tmp/300F.csv
(rimesfrankdata.sh -b $beginDate -e $prevDay 2>&1) | tee $logDir/rimesfrankdata.log 

# RBC Monthly data
mailTarget=$ownerMail
echo "RBC Monthly data"
ppMonthlyIndexReturns.sh $noSendArg -d $prevDay 2>&1 | tee $logDir/ppMonthlyIndexReturns.log | mailx -s "RBC Montly data" $ownerMail

#mailTarget=$ownerMail
# No Longer required as per Elly Grace
# Monthly AMV of XAO and XSO for Colonial First State
#if [ $sendMail -eq 1 ]; then
#	mailTarget="Research&PerformanceInformation@colonialfirststate.com.au,$ownerMail"
#fi
#histrep -d $prevDay -C -FA XAO 1> /tmp/CFS_XAO.csv 2> $logDir/CFS_XAO.log
#mail.py -s "AMV for All Ordinaries -Colonial" -t $mailTarget /tmp/CFS_XAO.csv
#histrep -d $prevDay -C -FA XSO 1> /tmp/CFS_XSO.csv 2> $logDir/CFS_XSO.log
#mail.py -s "AMV for Small Ordinaries -Colonial" -t $mailTarget /tmp/CFS_XSO.csv

# CFS Monthly I-AO Attribution Files
#echo Colonial First State - Monthly Attribution Data
#fsDoMonthlyGICS.sh $noSendArg -d $prevDay 2>&1 | tee $logDir/fsDoMonthlyGICS.log | mailx -s "First State Monthly Data Done (GICS)" $ownerMail

# BT -- Monthly GICS Changes
echo "BT -- Monthly GICS Changes"
( btMonthlyGicsChanges.sh $noSendArg -d $today 2>&1 ) | tee $logDir/btMonthlyGicsChanges.log | mailx -s "BT -- Monthly GICS Changes" $ownerMail

# Perennial Index Values XKO,B-3CX50,XJO,XSO,XTO
# These appear to be going to Perennial Value Management
(perennialDoMonthlyXKO.py $noSendArg -b $prevMonthBegin -e $today -i XKO 2>&1) | tee $logDir/perennialDoMonthlyXKO_XKO.log | mailx -s "Perennial Monthly XKO Data" $ownerMail
(perennialDoMonthlyXKO.py $noSendArg -b $prevMonthBegin -e $today -i XTO 2>&1) | tee $logDir/perennialDoMonthlyXKO_XTO.log | mailx -s "Perennial Monthly XTO Data" $ownerMail
(perennialDoMonthlyXKO.py $noSendArg -b $prevMonthBegin -e $today -i XJO 2>&1) | tee $logDir/perennialDoMonthlyXKO_XJO.log | mailx -s "Perennial Monthly XJO Data" $ownerMail
(perennialDoMonthlyXKO.py $noSendArg -b $prevMonthBegin -e $today -i XSO 2>&1) | tee $logDir/perennialDoMonthlyXKO_XSO.log | mailx -s "Perennial Monthly XSO Data" $ownerMail
(perennialDoMonthlyXKO.py $noSendArg -b $prevMonthBegin -e $today -i XAO 2>&1) | tee $logDir/perennialDoMonthlyXKO_XAO.log | mailx -s "Perennial Monthly XAO Data" $ownerMail
(perennialDoMonthlyXKO.py $noSendArg -b $prevMonthBegin -e $today -i XPK 2>&1) | tee $logDir/perennialDoMonthlyXKO_XPK.log | mailx -s "Perennial Monthly XPK Data" $ownerMail
(perennialDoMonthlyXKO.py $noSendArg -b $prevMonthBegin -e $today -i XPJ 2>&1) | tee $logDir/perennialDoMonthlyXKO_XPJ.log | mailx -s "Perennial Monthly XPJ Data" $ownerMail

# Wilson HTM monthly files
echo "Wilson HTM monthly files"
( wilsonWeightsReturns.sh $noSendArg -b $beginDate -e $prevDay 2>&1 ) | tee $logDir/wilsonWeightsReturns.log | mailx -s "Wilson HTM monthly files" $ownerMail

# UniSuper monthly after tax files
echo "UniSuper monthly after tax files"
( unisuperMonthly.sh $noSendArg 2>&1 ) | tee $logDir/unisuperMonthly.log | mailx -s "UniSuper monthly after tax files" $ownerMail

# SunSuper monthly after tax files
echo "SunSuper monthly after tax files"
( sunsuperAfterTaxData.sh $noSendArg -b $beginDate -e $prevDay 2>&1 ) | tee $logDir/sunsuperAfterTaxData.sh.log | mailx -s "SunSuper monthly after tax files" $ownerMail

# Perpetual monthly after tax files
echo "Perpetual monthly after tax files"
( perpetualAfterTaxData.sh $noSendArg -b $beginDate -e $prevDay 2>&1 ) | tee $logDir/perpetualAfterTaxData.sh.log | mailx -s "Perpetual monthly after tax files" $ownerMail

# Catholic monthly after tax files
echo "Catholic monthly after tax files"
( catholicAfterTaxData.sh $noSendArg -e $prevDay 2>&1 ) | tee $logDir/catholicAfterTaxData.sh.log | mailx -s "Catholic monthly after tax files" $ownerMail

# State Super monthly after tax files
echo "State Super monthly after tax files"
( statesuperAfterTaxData.sh $noSendArg -b $beginDate -e $prevDay 2>&1 ) | tee $logDir/statesuperAfterTaxData.sh.log | mailx -s "State Super monthly after tax files" $ownerMail

# Cooper monthly after tax files
echo "Cooper monthly after tax files"
( cooperAfterTaxData.sh $noSendArg -b $beginDate -e $prevDay 2>&1 ) | tee $logDir/cooperAfterTaxData.sh.log | mailx -s "Cooper monthly after tax files" $ownerMail

echo "BNP Paribas Franked Index Data"
( bnpMonthlyDivYield.sh $noSendArg -d $prevDay 2>&1 ) | tee $logDir/bnpMonthlyDivYield.log | mailx -s "BNP Paribas Div Yield" $ownerMail

echo "Challenger franking percentage data for I-200"
( challengerFrankPercent.sh $noSendArg -i I-200 -c XJO 2>&1 ) | tee $logDir/challengerFrankPercent.log | mailx -s "Challenger franking percentage data for I-200" $ownerMail

echo "Challenger franking percentage data for B-JMPT"
( challengerFrankPercent.sh $noSendArg -i B-JMPT -c XJMPT 2>&1 ) | tee $logDir/challengerFrankPercent-B-JMPT.log | mailx -s "Challenger franking percentage data for B-JMPT" $ownerMail

echo "Challenger franking percentage data for XKO"
( challengerFrankPercent.sh $noSendArg -i I-300 -c XKO 2>&1 ) | tee $logDir/challengerFrankPercent-XKO.log | mailx -s "Challenger franking percentage data for XKO" $ownerMail

# CBUS monthly after tax files
echo "CBUS monthly after tax files"
cbusAfterTaxData.sh $noSendArg -b $beginDate -e $prevDay > $logDir/cbusAfterTaxData.sh.log 2>&1

# following line should be the last line of this script
# because it checks for potential errors in the output
# produced by all the jobs
egrep -i "no|err|fail|ille|unab|segm|warn|trace|core|denied|get_old_prices|timed out|time out|abort|deadlock|Duplicate entries in|can't|don't|must be > 0|test" $logDir/* | egrep -vi 'keyboard|nov|extra|B-FTAU|Translists|DbaseGetCashPosition|cannonical|Fill|Ooh, err,|E-SNO|E-PNO|E-BNO|E-GDNO|E-BNOOA|E-MANO|E-CNNOA|E-MMNO|E-ANO|E-GYNO|E-GPNOA|E-BNOOB|E-TJNO|E-ERNO|E-LINO|E-WTNO|E-HZNO|E-NOD|E-ATNOA|E-PNOOA|E-BWNO|E-BCNO|E-AHNO|E-CLNO|E-PUNO|E-TXNO|E-SNO|E-PNO|E-BNO|E-GDNO|E-BNOOA|E-MANO|E-CNNOA|E-MMNO|E-ANO|E-GYNO|E-GPNOA|E-BNOOB|E-TJNO|E-ERNO|E-LINO|E-WTNO|E-HZNO|E-NOD|E-ATNOA|E-PNOOA|E-BWNO|E-BCNO|E-AHNO|E-CLNO|E-PUNO|E-TXNO|E-XENO|native python|DeprecationWarning|deferredDelivery| = pack\(|No of Trades|Tolhurst|Merrill|Reynolds|No changes|NOD|nonibWeeklyData.py|B-JQUIT|-nonreinvested|Successfully transferred' > /tmp/errors.monthlyJobs
if [ `wc -l /tmp/errors.monthlyJobs | awk '{print $1}'` -ne 0 ]; then
    warn "Potential errors exist in monthly jobs"
    cat /tmp/errors.monthlyJobs | mailx -s "monthlyJobs: potential errors from log directory" $ownerMail
fi

echo "monthly jobs done"

