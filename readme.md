# This repo contains bash commands for scheduled tasks that run on qds-devel2

## File structure

1. tasks.sh     # replace morningJobs.sh
2. env.sh  # bash command to replicate the devel2 node with WSL-Ubunutu distro on local machine
3. .bash_proifle/.bashrc   # copy from devel2 node
4. afternoonJobs/monthlyJobs.sh # copy from devel2 node
5. qds_machine.exp # expect script to connect to qds nodes
6. read_config.py # copy from devel2 to put in Ubuntu distro to allow the script to read configuration file. 


## Features

tasks.sh reorganized morningJobs.sh to seal commands for each client to separate functions and call them accordingly based on the task parameter.  <br/>

To log the start time an end time for each task, function *timestamp* is called before and after the client module to echo the time and event to a universe log file. Function *logger* takes the keyword and client function as parameters, and will create a log with the same name of the keyword under $log_Dir, and create error log under tmp folder. <br/>

Please refer to the event_logger repo for details about error identification and sending out alerts <br/>

## TEST run
Use command like below:
```bash
./debug_tasks.sh -n -d 20180403 -t 3
```

## Capture errors in exit early
key commands 
echo client job start 
echo client job end

client:
command 1 -> error log $#
command 2  
command1 exit command  echo end

read log -> error ;

send incomplete; 