another_job () {
    echo "Working on another job"
    # exit 1
    python './dummy.py'
    status=$?
    echo "exit code in another job: $status"
    echo "End of work"
    return $status
}


echo "Main Start"
sh ./job.sh 
echo "exit code: $?"

another_job
echo "exit code for another job: $?"
echo "Main End"
