#!/bin/sh
#
# $Header: /import/cvs/client/bin/morningJobs.sh,v 1.227 2020/04/23 06:00:10 qdsbuild Exp $
#----------------------------------------------------------------------------
#  SYNOPSIS
#	morningJobs.sh
#----------------------------------------------------------------------------
#  DESCRIPTION
#	Runs the list of jobs which are to be performed after the overnight
#	processing has been completed. 
#----------------------------------------------------------------------------
PATH=/usr/bin:/usr/local/bin:/usr/sfw/bin:/projects/qds/bin:/projects/client/bin:/projects/aftertax/bin
export PATH
PERLLIB=$PERLLIB:/projects/perl5
export PERLLIB
. /opt/sybase_15_0_3_OC/SYBASE.sh

# Source QDS common script first
common_dir=`read_config.py -s common:dir`
. $common_dir/utility.sh

today=`dateformat -i`
prevDay=`dateformat -P $today`
sendMail=1
republish=0
ownerMail=`read_config.py -s client:alertmail`
noSendArg=""
non_trading_file=`read_config.py -s nontradingfile`
baseDir=`read_config.py -s client:data_dir`
logDir=`read_config.py -s client:log_dir`
logDir=$logDir/morning
task=0
echo log directory is $logDir

rm /tmp/errors.monthlyJobs

# log file to capture timing info
timeLog=$logDir/timestamps.log
rm -f $timeLog
timestamp () {
	stamp=`date '+%Y-%m-%d %H:%M:%S'`
	echo "$@: $stamp" >> $timeLog
}

# takes name and function
logger () {
        timestamp "$1 - START"
        ($2 $1)
        # log errors
        ret_code=$?
        if [[ -f "$logDir/$1.log" ]]; then
            egrep -i "no|err|fail|ille|unab|segm|warn|trace|core|denied|get_old_prices|timed out|time out|abort|deadlock|Duplicate entries in|530 Login incorrect|can't|don't|must be > 0|test" $logDir/$1.log | egrep -vi 'keyboard|nov|extra|B-FTAU|Translists|DbaseGetCashPosition|cannonical|Fill|Ooh, err,|E-SNO|E-PNO|E-BNO|E-GDNO|E-BNOOA|E-MANO|E-CNNOA|E-MMNO|E-ANO|E-GYNO|E-GPNOA|E-BNOOB|E-TJNO|E-ERNO|E-LINO|E-WTNO|E-HZNO|E-NOD|E-ATNOA|E-PNOOA|E-BWNO|E-BCNO|E-AHNO|E-CLNO|E-PUNO|E-TXNO|E-SNO|E-PNO|E-BNO|E-GDNO|E-BNOOA|E-MANO|E-CNNOA|E-MMNO|E-ANO|E-GYNO|E-GPNOA|E-BNOOB|E-TJNO|E-ERNO|E-LINO|E-WTNO|E-HZNO|E-NOD|E-ATNOA|E-PNOOA|E-BWNO|E-BCNO|E-AHNO|E-CLNO|E-PUNO|E-TXNO|E-XENO|native python|DeprecationWarning|deferredDelivery| = pack\(|No of Trades|Tolhurst|Merrill|Reynolds|No changes|NOD|nonibWeeklyData.py|B-JQUIT|\[additional.txt\]|no data to send' > /tmp/errors.monthlyJobs.$1.log
			cat /tmp/errors.monthlyJobs.$1.log >> /tmp/errors.monthlyJobs
        fi
        # if return code found, write error message
        if [ $ret_code -ne 0 ]; then
            echo "ERROR exit code found for $1"
            echo  "ERROR: exit code $ret_code for task: $1" >> /tmp/errors.morningJobs.$1.log
        fi
        timestamp "$1 - END"
}

Usage="Usage: $0 [-d date] [-n] [-m address] [-R]
-d date: Defaults to current date 
-n: Don't send data emails to clients (internal status mail are still sent)
-m address: Internal status mail address (default to $ownerMail)
-R : republish
-t : default as zero to run all tasks
    5) gics_perpetual;;
    6) perpetual_300gics;;
    8) bt_daily_aftertax_files;;
    10) cbus_daily_aftertax_files;;
    11) cfsdaily_gics;;
    12) mercury_daily_gics;;
    13) fs_do_daily_gics;;
    14) bt_send_alerts_sde;;
    15a) bt_back_office_load_no_prop_split;;
    16a) bt_prelim_index_data_no_prop_split;;
    17) cfs_data;;
    19) black_rock_index_value;;
    20) wilson_daily_files;;
    23) daily_warakirri_upload;;
    24) warakirri_index_data;;
    26) cc_data;;
    27) bnp_daily_franked_data;;
    28) challengerFrankPercent;;
    33) long_reach_index_value;;
    29) atb_check_ratio;;
    30) atb_data_flag;;
    31) solaris_wealth_attribution;;
    32) defect_check;;
"

while getopts "t:m:d:R:n" c ; do
	case $c in
        t) task=$OPTARG;;
		m) ownerMail=$OPTARG;;
		d) today=$OPTARG;;
		R) republish=1;;
        n) sendMail=0;;
		*) echo "$Usage"; 
		exit 1;;
	esac
done



if [ $sendMail -eq 0 ]; then
	noSendArg="-n"
fi

# check that overnight processing has finished

# note: if Monday, date on file may be from Saturday or Sunday.  If that is so,
# then the file will already exist since the run was done on the weekend, and
# this is called on Monday. 

today=`dateformat -i $today`
echo today is $today
prevDay=`dateformat -P $today`
prevWeek=`dateformat -W -1 $today`
Dow=`dateformat -c $today | awk '{print $1};'`

if [ `dateformat -p $today` != `dateformat -P $today` ]; then
	isMonday="TRUE"
else
	isMonday="FALSE"
fi

flagfile_dir="`read_config.py -s dbaseload_dir`/data/flagfiles"
satFile=${flagfile_dir}/`dateformat -p -p -i $today`.7am
sunFile=${flagfile_dir}/`dateformat -p -i $today`.7am
todayFile=${flagfile_dir}/`dateformat -i $today`.7am
echo todayFile is $todayFile
echo isMonday is $isMonday
echo satFile is $satFile
echo sunFile is $sunFile

if [ "$isMonday" != "TRUE"  -o \( ! -r $satFile -a ! -r $sunFile \) ];  then
	# check to see if overnight processing has been done for today
	echo "waiting for overnight processing"
	if [ `dateformat -i` -eq $today ] ; then
		while [ ! -r $todayFile -a `date +%H%M` -lt 0900 ]
		do
			echo "Sleeping"
			sleep 300
		done
	fi

	if [ ! -r $todayFile -a `date +%H%M` -ge 0900 ] ; then
		echo "**ERROR** Waited until 9:00 for ${flagfile_dir}/${today}.7am to appear!!" | tee $logDir/morningJobs.log | mailx -s "HELP! client morning jobs" $ownerMail
        exit 1
    fi
fi

# (3) CFS
# No longer required as per Wang Fei from Colonial state
# mailTarget=$ownerMail
#echo "Custom indices for Colonial First State - STORM and Ex-CBA"
#if [ $sendMail -eq 1 ]; then
#	mailTarget="CFSFundAccounting@cba.com.au,$ownerMail"
#fi
#histrep -C -d `dateformat -P $today` -FX B-JXCB B-JXCB2 B-100MP B-100MP2 B-KIMP B-KIMP2 B-KMPT B-KMPT2 1> /tmp/CFS_Custom.csv 2> $logDir/CFS_Custom.log
#mail.py -s "XAO ex CBA, XTO ex Prop, 300 ex Prop" -t $mailTarget /tmp/CFS_Custom.csv

# (4) CFS
# No Longer Required as per Elly Grace
# mailTarget=$ownerMail
#echo "300I ex Property"
#if [ $sendMail -eq 1 ]; then
#	mailTarget="Research&PerformanceInformation@colonialfirststate.com.au,mtran@colonialfirststate.com.au,aly@colonialfirststate.com.au,$ownerMail"
#fi
#histrep -d `dateformat -P $today` -F X B-KIMP B-KIMP2 B-KMPT B-KMPT2 1> /tmp/CFS_KIMP_KMPT.csv 2> $logDir/CFS_KIMP_KMPT.log
#mail.py -s "300I less XPT" -t $mailTarget /tmp/CFS_KIMP_KMPT.csv


gics_perpetual () {
    # (5) GICS for perpetual for rothschild/perpetual investments
    #
    # timestamp "GICS for perpetual for rothschild/perpetual investments - START"
    echo "GICS for perpetual for rothschild/perpetual investments"
    for mailPeople in rbcdexiaperformance@rbcdexia.com austperformance@jpmorgan.com; do 
        perpDoDaily300Gics.sh $noSendArg -d `dateformat -P $today` -m $mailPeople 2>&1 | tee $logDir/$1.log | mailx -s "Perpetual/Rothschild GICS 4pm daily (perpDoDaily300Gics.sh)" $ownerMail
    done
    # timestamp "GICS for perpetual for rothschild/perpetual investments - END"
}

perpetual_300gics () {
    # (6) Perpetual - 300 GICS SECTOR Changes
    #
    # timestamp "Perpetual - 300 GICS SECTOR Changes - START"
    echo Perpetual - 300 GICS SECTOR Changes
    # timestamp "perpIndexChanges.py START"
    perpIndexChanges.py $noSendArg -d $prevDay 2>&1 | tee $logDir/$1.log | mailx -s "Perpetual 300 GICS Changes" $ownerMail
    # timestamp "perpIndexChanges.py END"
    # timestamp "Perpetual - 300 GICS SECTOR Changes - END"
}

bt_daily_aftertax_files () {
    # (8) BT daily after tax files
    #
    # timestamp "BT daily after tax files - START"
    echo "BT daily after tax files"
    ( btAfterTaxData.sh $noSendArg 2>&1 ) | tee $logDir/$1.log | mailx -s "BT - daily after tax files" $ownerMail
    # timestamp "BT daily after tax files - END"
}


cbus_daily_aftertax_files () {
    # (10) CBUS Orbis daily after tax files
    #
    # timestamp "CBUS Orbis daily after tax files - START"
    echo "CBUS Orbis daily after tax files"
    ( cbusAfterTaxDataReinvested.sh -D $noSendArg 2>&1 ) | tee $logDir/$1.log | mailx -s "CBUS Orbis daily after tax files" $ownerMail
    # timestamp "CBUS Orbis daily after tax files - END"
}

cfsdaily_gics() {
    # (11) CFS - send accumulation indices (currently only in morning, but will change)

    echo CFS accumulation
    # timestamp "cfsdailyGICS.sh START"
    cfsdailyGICS.sh $noSendArg -u -a -d $prevDay 2>&1 | tee $logDir/$1.log | mailx -s "CFS" $ownerMail
    # timestamp "cfsdailyGICS.sh END"
}

mercury_daily_gics () {
    # (12) Mercury GICS attribution files
    #
    # timestamp "mamDoDaily300Gics.sh START"
    mamDoDaily300Gics.sh $noSendArg -d $prevDay 2>&1 | tee $logDir/$1.log | mailx -s "Mercury GICS daily (mamDoDaily300Gics.sh)" $ownerMail
    # timestamp "mamDoDaily300Gics.sh END"
}

fs_do_daily_gics () {
    # (13) Colonial First State - daily data for the stocks in XAO and gicsgroup info 
    # FirstState GICS attribution files
    #
    echo Colonial First State - daily attribution data 
    # timestamp "fsDoDailyGICS.log START"
    fsDoDailyGICS.sh $noSendArg -d $prevDay 2>&1 | tee $logDir/$1.log | mailx -s "First State data done (GICS)" $ownerMail
    # timestamp "fsDoDailyGICS.log END"
}

bt_send_alerts_sde () {
    # (14) BT - 150 ex 50 index changes check
    #
    echo "BT - 150 ex 50 index changes check"
    # timestamp "bt_send_alerts_sde.py START"
    bt_send_alerts_sde.py -d $prevDay 2>&1 | tee $logDir/$1.log | mailx -s "BT - 150 ex 50 index changes check" $ownerMail
    # timestamp "bt_send_alerts_sde.py END"
}

bt_back_office_load_no_prop_split () {
    # (15a) BT - Backoffice Data (FMC + Extra) NO PROP SPLIT
    #
    echo "BT - Backoffice Data NO PROP SPLIT"        
    # timestamp "btBackOfficeLoad.sh NO PROP SPLIT START"
    btBackOfficeLoad.sh $noSendArg -u -h -d $prevDay 2>&1 | tee $logDir/$1.log | mailx -s "BT - 9am Backoffice Data (no prop split)" $ownerMail

    if [ $Dow = "Tue" ]; then
        # Sundays Data
        btBackOfficeLoad.sh $noSendArg -u -h -d `dateformat -p $prevDay` 2>&1 | tee $logDir/$1.log | mailx -s "BT - 9am Backoffice Data (no prop split)" $ownerMail

        # Saturdays Data
        btBackOfficeLoad.sh $noSendArg -u -h -d `dateformat -pp $prevDay` 2>&1 | tee $logDir/$1.log | mailx -s "BT - 9am Backoffice Data (no prop split)" $ownerMail
    fi
    # timestamp "btBackOfficeLoad.sh NO PROP SPLIT END"
}

bt_prelim_index_data_no_prop_split () {
    # (16a) BT NO PROP SPLIT
    #
    echo "BT - Preliminary Custom Index Data NO PROP SPLIT"        
    # timestamp "btPrelimIndexData.sh NO PROP SPLIT START"
    btPrelimIndexData.sh $noSendArg -d $today 2>&1 | tee $logDir/$1.log | mailx -s "BT - Preliminary Custom Index data (no prop split)" $ownerMail
    # timestamp "btPrelimIndexData.sh END NO PROP SPLIT END"
}

cfs_data () {
    # (17) CFS
    #
    echo "CFS Market Data"
    # timestamp "cfsData.sh START"
    ( cfsData.sh $noSendArg -c -d $prevDay 2>&1 | tee $logDir/$1.log | mailx -s "CFS -- Market Data" $ownerMail )
    # timestamp "cfsData.sh END"
}

black_rock_index_value () {
    # (19) Blackrock - index values
    #
    echo Blackrock - index values
    # timestamp "blackrockIndexValues.sh START"
    ( blackrockIndexValues.sh $noSendArg -b `dateformat -PPP $prevDay` -e $prevDay 2>&1 ) | tee $logDir/$1.log | mailx -s "Blackrock Index Values - Daily" $ownerMail
    # timestamp "blackrockIndexValues.sh END"
}

wilson_daily_files () {
    # (20) Wilson HTM daily files
    #
    # timestamp "Wilson HTM daily files START"
    echo "Wilson HTM daily files"
    ( wilsonWeightsReturnsDaily.sh $noSendArg -d $prevDay 2>&1 ) | tee $logDir/$1.log | mailx -s "Wilson HTM daily files" $ownerMail
    # timestamp "Wilson HTM daily files END"
}

daily_warakirri_upload () {
    # (23) Daily Warakirri FMC
    #
    echo "Daily Warakirri Upload"
    # timestamp "warakirriFMCFiles.sh START"
    ( warakirriFMCFiles.sh $noSendArg -d $today 2>&1 ) | tee $logDir/$1.log | mailx -s "Daily Warakirri Upload" $ownerMail
    # timestamp "warakirriFMCFiles.sh END"
}

warakirri_index_data () {
    # (24) Warakirri index data
    #
    echo "After-tax ginraw data for Warakirri"
    # timestamp "warakirriIndexData.sh START"
    ( warakirriIndexData.sh $noSendArg -d $today 2>&1 ) | tee $logDir/$1.log | mailx -s "After-tax ginraw data for Warakirri" $ownerMail
    # timestamp "warakirriIndexData.sh END"
}

cc_data () {
    # (26) Continuum Capital
    #
    echo "Continuum Capital Market Data"
    # timestamp "ccData.sh START"
    ( ccData_post_mis_decommission.sh $noSendArg -d $prevDay 2>&1 ) | tee $logDir/$1.log | mailx -s "Continuum Capital Market Data" $ownerMail
    # timestamp "ccData.sh END"
}

bnp_daily_franked_data () {
    # (27) BNP Paribas
    #
    echo "BNP Paribas Franked Index Data"
    # timestamp "bnpDailyFrankedData.sh START"
    ( bnpDailyFrankedData.sh $noSendArg -d $prevDay 2>&1 ) | tee $logDir/$1.log | mailx -s "BNP Paribas Franked Indices" $ownerMail
    # timestamp "bnpDailyFrankedData.sh END"
}

challenger_frank_percent () {
    # (28) Challenger
    #
    # timestamp "Challenger franking percentage data for I-200 START"
    echo "Challenger franking percentage data for I-200"
    ( challengerFrankPercent.sh $noSendArg -i I-200 -c XJO -b $prevDay -e $prevDay 2>&1 ) | tee $logDir/$1.log | mailx -s "Challenger franking percentage data for I-200" $ownerMail
    # timestamp "Challenger franking percentage data for I-200 END"
}


long_reach_index_value () {
    # (33) Longreach
    #
    echo "Longreach index value data for I-300"
    ( catholicAfterTaxData.sh $noSendArg -D 1 -b $prevDay -e $prevDay 2>&1 ) | tee $logDir/$1.log | mailx -s "Longreach index value data for I-300" $ownerMail
}


# other tasks
# TODO: confirm if they need to be run for every task
######### After tax data and flags - Begin ###############

atb_check_ratio () {
    # after tax benchmark - Ratio file check
    echo "After tax benchmark - Ratio file check"
    # timestamp "atbCheckRatios.sh START"
    atbCheckRatios.sh
    # timestamp "atbCheckRatios.sh END"
}


atb_data_flag () {
    # after tax benchmark - data flags
    echo "After tax benchmark - data flag"
    # timestamp "ATB data flag - START"
    fn=`read_config.py -s aftertax:data_dir`/flagfiles/${prevDay}.Data.Confirm
    touch $fn
    if [ $? -ne 0 ]; then
        echo ERROR: Could not touch flag file ${fn} | tee $logDir/$1.log | mailx -s "ATB - Data flag" $ownerMail
    else
        echo Set flag: ${fn} | tee $logDir/$1.log | mailx -s "ATB - Data flag" $ownerMail
    fi
    # timestamp "ATB data flag - END"
}

########## After tax data and flags - End ###############

solaris_wealth_attribution () {
    #########################################
    ########## Solaris Attribution ##########
    #########################################
    echo "Solaris Wealth - Attribution"
    # timestamp "Solaris Wealth - Attribution - START"
    ( LOGNAME=swealth; export LOGNAME; /projects/attribution/swealth/bin/FileChecksAndReformat.sh -w -d ${prevDay} 2>&1 ) | tee $logDir/FileChecksAndReformat.log | mailx -s "Solaris Wealth - Attribution - FileChecksAndReformat" $ownerMail
    if [ `egrep 'WARN|ERROR' $logDir/FileChecksAndReformat.log | egrep -v "WARNING: There is no transactions" | wc -l ` -ne 0 ]; then
        warn "Solaris Wealth attribution load error"
    else
        ( LOGNAME=swealth; export LOGNAME; /projects/attribution/swealth/bin/DailyReports.sh -d ${prevDay} 2>&1 ) | tee $logDir/DailyReports.log | mailx -s "Solaris Wealth - Attribution - DailyReports" $ownerMail
        # timestamp "Solaris Wealth - Attribution - END"
    fi
}

defect_check ()  {
    # following line should be the last line of this script
    # because it checks for potential errors in the output
    # produced by all the jobs
    # egrep -i "wrong|out of order|not|err|fail|ille|unab|segm|warn|trace|core|denied|get_old_prices|timed out|time out|abort|deadlock|Duplicate entries in|can't|don't|must be > 0|test" $logDir/* | egrep -iv 'deprecat|FileChecksAndReformat.log|no change|no dividend|notification.py|_latest|N do not mail to clients|no data to send|Cannot get \[Cashflows|DailyReports.log' > /tmp/errors.morningJobs
    if [ `wc -l /tmp/errors.morningJobs | awk '{print $1}'` -ne 0 ]; then
        warn "Potential errors exist in morning jobs"
        cat /tmp/errors.morningJobs | mailx -s "morningJobs: potential errors from log directory" $ownerMail
    fi
}


case $task in
    5) logger 'gics_perpetual' gics_perpetual;;
    6) logger 'perpetual_300gics' perpetual_300gics;;
    8) logger 'bt_daily_aftertax_files' bt_daily_aftertax_files;;
    10) logger 'cbus_daily_aftertax_files' cbus_daily_aftertax_files;;
    11) logger 'cfsdaily_gics' cfsdaily_gics;;
    12) logger 'mercury_daily_gics' mercury_daily_gics;;
    13) logger 'fs_do_daily_gics' fs_do_daily_gics;;
    14) logger 'bt_send_alerts_sde' bt_send_alerts_sde;;
    15a) logger 'bt_back_office_load_no_prop_split' bt_back_office_load_no_prop_split;;
    16a) logger 'bt_prelim_index_data_no_prop_split' bt_prelim_index_data_no_prop_split;;
    17) logger 'cfs_data' cfs_data;;
    19) logger 'black_rock_index_value' black_rock_index_value;;
    20) logger 'wilson_daily_files' wilson_daily_files;;
    23) logger 'daily_warakirri_upload' daily_warakirri_upload;;
    24) logger 'warakirri_index_data' warakirri_index_data;;
    26) logger 'cc_data' cc_data;;
    27) logger 'bnp_daily_franked_data' bnp_daily_franked_data;;
    28) logger 'challenger_frank_percent' challenger_frank_percent;;
    33) logger 'long_reach_index_value' long_reach_index_value;;
    29) logger 'atb_check_ratio' atb_check_ratio;;
    30) logger 'atb_data_flag' atb_data_flag;;
    31) logger 'solaris_wealth_attribution' solaris_wealth_attribution;;
    32) logger 'defect_check' defect_check;;
    *) echo "Curret task num: $task"; 
esac

if [ $task -eq 0 ]; then
    logger 'gics_perpetual' gics_perpetual
    logger 'perpetual_300gics' perpetual_300gics
    logger 'bt_daily_aftertax_files' bt_daily_aftertax_files
    logger 'cbus_daily_aftertax_files' cbus_daily_aftertax_files
    logger 'cfsdaily_gics' cfsdaily_gics
    logger 'mercury_daily_gics' mercury_daily_gics
    logger 'fs_do_daily_gics' fs_do_daily_gics
    logger 'bt_send_alerts_sde' bt_send_alerts_sde
    logger 'bt_back_office_load_no_prop_split' bt_back_office_load_no_prop_split
    logger 'bt_prelim_index_data_no_prop_split' bt_prelim_index_data_no_prop_split
    logger 'cfs_data' cfs_data
    logger 'black_rock_index_value' black_rock_index_value
    logger 'wilson_daily_files' wilson_daily_files
    logger 'daily_warakirri_upload' daily_warakirri_upload
    logger 'warakirri_index_data' warakirri_index_data
    logger 'cc_data' cc_data
    logger 'bnp_daily_franked_data' bnp_daily_franked_data
    logger 'challenger_frank_percent' challenger_frank_percent
    logger 'long_reach_index_value' long_reach_index_value
    logger 'atb_check_ratio' atb_check_ratio
    logger 'atb_data_flag' atb_data_flag
    logger 'solaris_wealth_attribution' solaris_wealth_attribution
    # logger 'defect_check' defect_check
fi
