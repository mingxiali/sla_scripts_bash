#!/bin/sh
#----------------------------------------------------------------------------
#  SYNOPSIS
#	monthlyJobs.sh
#----------------------------------------------------------------------------
#  DESCRIPTION
#	Runs the list of jobs which are to be performed on the first of the month 
#	after the overnight processing has been completed. 
#----------------------------------------------------------------------------
# $Header: /import/cvs/client/bin/monthlyJobs.sh,v 1.92 2021/01/04 02:42:12 qdsbuild Exp $
#----------------------------------------------------------------------------

PATH=/usr/bin:/usr/local/bin:/usr/sfw/bin:/projects/qds/bin:/projects/client/bin:/projects/aftertax/bin
export PATH
PERLLIB=$PERLLIB:/projects/perl5
export PERLLIB
. /opt/sybase_15_0_3_OC/SYBASE.sh

# Source QDS common script first
common_dir=`read_config.py -s common:dir`
. $common_dir/utility.sh

today=`dateformat -i`
sendMail=1
ownerMail=`read_config.py -s client:alertmail`
dataDir=`read_config.py -s client:data_dir`
logDir=`read_config.py -s client:log_dir`
logDir=$logDir/monthly
task=0
echo log directory is $logDir
echo data directory is $dataDir

# log file to capture timing info
timeLog=$logDir/timestamps.log
rm -f $timeLog

rm /tmp/errors.monthlyJobs

timestamp () {
	stamp=`date '+%Y-%m-%d %H:%M:%S'`
	echo "$@: $stamp" >> $timeLog
}

# takes name and function
logger () {
        timestamp "$1 - START"
        ($2 $1)
        # log errors
        ret_code=$?
        if [[ -f "$logDir/$1.log" ]]; then
            egrep -i "no|err|fail|ille|unab|segm|warn|trace|core|denied|get_old_prices|timed out|time out|abort|deadlock|Duplicate entries in|can't|don't|must be > 0|test" $logDir/$1.log | egrep -vi 'keyboard|nov|extra|B-FTAU|Translists|DbaseGetCashPosition|cannonical|Fill|Ooh, err,|E-SNO|E-PNO|E-BNO|E-GDNO|E-BNOOA|E-MANO|E-CNNOA|E-MMNO|E-ANO|E-GYNO|E-GPNOA|E-BNOOB|E-TJNO|E-ERNO|E-LINO|E-WTNO|E-HZNO|E-NOD|E-ATNOA|E-PNOOA|E-BWNO|E-BCNO|E-AHNO|E-CLNO|E-PUNO|E-TXNO|E-SNO|E-PNO|E-BNO|E-GDNO|E-BNOOA|E-MANO|E-CNNOA|E-MMNO|E-ANO|E-GYNO|E-GPNOA|E-BNOOB|E-TJNO|E-ERNO|E-LINO|E-WTNO|E-HZNO|E-NOD|E-ATNOA|E-PNOOA|E-BWNO|E-BCNO|E-AHNO|E-CLNO|E-PUNO|E-TXNO|E-XENO|native python|DeprecationWarning|deferredDelivery| = pack\(|No of Trades|Tolhurst|Merrill|Reynolds|No changes|NOD|nonibWeeklyData.py|B-JQUIT|\[additional.txt\]|no data to send|paid for ZNO|paid for ANO|nonreinvested.csv' > /tmp/errors.monthlyJobs.$1.log
			cat /tmp/errors.monthlyJobs.$1.log >> /tmp/errors.monthlyJobs
        fi
        # if return code found, write error message
        if [ $ret_code -ne 0 ]; then
            echo "ERROR exit code found for $1"
            echo  "ERROR: exit code $ret_code for task: $1" >> /tmp/errors.monthlyJobs.$1.log
        fi
        timestamp "$1 - END"
}

Usage="Usage: $0 [-d date] [-n] [-m address]
    -d date: Defaults to current date 
    -n: Don't send data emails to clients (internal status mail are still sent)
	-m address: Internal status mail address (default to $ownerMail)
	-t : default as zero to run all tasks
"

while getopts "t:m:d:n" c ; do
    case $c in
	t) task=$OPTARG;;
	m) ownerMail=$OPTARG;;
	d) today=$OPTARG;;
	n) sendMail=0;;
    *) echo "$Usage";
        exit 1;;
    esac
done

noSendArg=""

if [ $sendMail -eq 0 ]; then
    noSendArg="-n"
fi

# check that overnight processing has finished
# note: if Monday, or weekend, date on file may be from Saturday or Sunday.  

echo today is $today
prevDay=`dateformat -P $today`
prevMonth=`dateformat -M -1 $today`
prevMonthBegin=`echo $prevMonth | cut -c1-6`01


set `dateformat -c $today`
day=$1

if [ "$day" = "Sat" -o "$day" = "Sun" -o "$day" = "Mon" ]; then
	isWeekend="TRUE"
else
	isWeekend="FALSE"
fi

flagfile_dir="`read_config.py -s dbaseload_dir`/data/flagfiles"
prev2DayFile=${flagfile_dir}/`dateformat -p -p -i $today`.7am
prevDayFile=${flagfile_dir}/`dateformat -p -i $today`.7am
todayFile=${flagfile_dir}/`dateformat -i $today`.7am
echo todayFile is $todayFile
echo prevDayFile is $prevDayFile
echo prev2DayFile is $prev2DayFile

if [ "$day" != "Mon"  -o \( ! -r $prev2DayFile -a ! -r $prevDayFile \) ];  then
	if [ "$day" != "Sun"  -o \( ! -r $prevDayFile \) ];  then
		# check to see if overnight processing has been done for today
		echo "waiting for overnight processing"
		if [ `dateformat -i` -eq $today ] ; then
			while [ ! -r $todayFile -a `date +%H%M` -lt 1700 ]
			do
				echo "Sleeping"
        		sleep 300
			done
		fi

		if [ ! -r $todayFile -a `date +%H%M` -ge 1700 ] ; then
        	echo "**ERROR** Waited until 5:00pm for ${flagfile_dir}/${today}.7am to appear!!" | tee $logDir/monthlyJobs.log
        	echo "**ERROR** Waited until 5:00pm for ${flagfile_dir}/${today}.7am to appear!!  Need to rerun the monthly client jobs when the processing is complete" | tee -a $logDir/monthlyJobs.log | mailx -s "HELP! client monthly jobs" $ownerMail
        	exit 1
		fi
	fi
fi

echo "OK - ready to go"

beginDate=`dateformat -M -1 $today`
beginDate=`dateformat -P $beginDate`
beginDate=`dateformat -N $beginDate`

rimes_frank_data () {
	# (1) 300 franked index to RBC
	mailTarget=$ownerMail
	echo "300 franked index to RBC"
	if [ $sendMail -eq 1 ]; then
		mailTarget=rbcausperformance@rbc.com,dxaup@rbc.com,gts.ap.mo.performance@imcap.ap.ssmb.com,Brian.OMarnain@bennelongfunds.com,aravinda.jayaram@citi.com
	fi
	histrep -C -n -b $beginDate -e $prevDay -FX B-300F2 | tee $logDir/Bennelong-300F2.log > /tmp/300F.csv
	mail.py -s "300 Franked Index" -t $ownerMail -b $mailTarget /tmp/300F.csv
	(rimesfrankdata.sh -b $beginDate -e $prevDay 2>&1) | tee $logDir/$1.log 
}

pp_monthly_index_return () {
	# (2) RBC Monthly data
	mailTarget=$ownerMail
	echo "RBC Monthly data"
	ppMonthlyIndexReturns.sh $noSendArg -d $prevDay 2>&1 | tee $logDir/$1.log | mailx -s "RBC Montly data" $ownerMail
}

#mailTarget=$ownerMail
# No Longer required as per Elly Grace
# Monthly AMV of XAO and XSO for Colonial First State
#if [ $sendMail -eq 1 ]; then
#	mailTarget="Research&PerformanceInformation@colonialfirststate.com.au,$ownerMail"
#fi
#histrep -d $prevDay -C -FA XAO 1> /tmp/CFS_XAO.csv 2> $logDir/CFS_XAO.log
#mail.py -s "AMV for All Ordinaries -Colonial" -t $mailTarget /tmp/CFS_XAO.csv
#histrep -d $prevDay -C -FA XSO 1> /tmp/CFS_XSO.csv 2> $logDir/CFS_XSO.log
#mail.py -s "AMV for Small Ordinaries -Colonial" -t $mailTarget /tmp/CFS_XSO.csv

# CFS Monthly I-AO Attribution Files
#echo Colonial First State - Monthly Attribution Data
#fsDoMonthlyGICS.sh $noSendArg -d $prevDay 2>&1 | tee $logDir/fsDoMonthlyGICS.log | mailx -s "First State Monthly Data Done (GICS)" $ownerMail
bt_monthly_gics_changes () {
	# (3) BT -- Monthly GICS Changes
	echo "BT -- Monthly GICS Changes"
	( btMonthlyGicsChanges.sh $noSendArg -d $today 2>&1 ) | tee $logDir/$1.log | mailx -s "BT -- Monthly GICS Changes" $ownerMail
}

perennial_monthly () {
	#  (4) Perennial Index Values XKO,B-3CX50,XJO,XSO,XTO
	# These appear to be going to Perennial Value Management
	(perennialDoMonthlyXKO.py $noSendArg -b $prevMonthBegin -e $today -i XKO 2>&1) | tee $logDir/perennialDoMonthlyXKO_XKO.log | mailx -s "Perennial Monthly XKO Data" $ownerMail
	(perennialDoMonthlyXKO.py $noSendArg -b $prevMonthBegin -e $today -i XTO 2>&1) | tee $logDir/perennialDoMonthlyXKO_XTO.log | mailx -s "Perennial Monthly XTO Data" $ownerMail
	(perennialDoMonthlyXKO.py $noSendArg -b $prevMonthBegin -e $today -i XJO 2>&1) | tee $logDir/perennialDoMonthlyXKO_XJO.log | mailx -s "Perennial Monthly XJO Data" $ownerMail
	(perennialDoMonthlyXKO.py $noSendArg -b $prevMonthBegin -e $today -i XSO 2>&1) | tee $logDir/perennialDoMonthlyXKO_XSO.log | mailx -s "Perennial Monthly XSO Data" $ownerMail
	(perennialDoMonthlyXKO.py $noSendArg -b $prevMonthBegin -e $today -i XAO 2>&1) | tee $logDir/perennialDoMonthlyXKO_XAO.log | mailx -s "Perennial Monthly XAO Data" $ownerMail
	(perennialDoMonthlyXKO.py $noSendArg -b $prevMonthBegin -e $today -i XPK 2>&1) | tee $logDir/perennialDoMonthlyXKO_XPK.log | mailx -s "Perennial Monthly XPK Data" $ownerMail
	(perennialDoMonthlyXKO.py $noSendArg -b $prevMonthBegin -e $today -i XPJ 2>&1) | tee $logDir/perennialDoMonthlyXKO_XPJ.log | mailx -s "Perennial Monthly XPJ Data" $ownerMail
	cat $logDir/perennialDoMonthly*.log > $logDir/$1.log
}

wilson_weights_returns () {
	# (5) Wilson HTM monthly files
	echo "Wilson HTM monthly files"
	( wilsonWeightsReturns.sh $noSendArg -b $beginDate -e $prevDay 2>&1 ) | tee $logDir/$1.log | mailx -s "Wilson HTM monthly files" $ownerMail
}

unisuper_monthly () {
	# (6) UniSuper monthly after tax files
	echo "UniSuper monthly after tax files"
	( unisuperMonthly.sh $noSendArg 2>&1 ) | tee $logDir/$1.log | mailx -s "UniSuper monthly after tax files" $ownerMail
}

sun_super_aftertax_data () {
	# (7) SunSuper monthly after tax files
	echo "SunSuper monthly after tax files"
	( sunsuperAfterTaxData.sh $noSendArg -b $beginDate -e $prevDay 2>&1 ) | tee $logDir/$1.log | mailx -s "SunSuper monthly after tax files" $ownerMail
}

perpetual_aftertax_data () {
	# (8) Perpetual monthly after tax files
	echo "Perpetual monthly after tax files"
	( perpetualAfterTaxData.sh $noSendArg -b $beginDate -e $prevDay 2>&1 ) | tee $logDir/$1.log | mailx -s "Perpetual monthly after tax files" $ownerMail
}

catholic_aftertax_data () {
	# (9) Catholic monthly after tax files
	echo "Catholic monthly after tax files"
	( catholicAfterTaxData.sh $noSendArg -e $prevDay 2>&1 ) | tee $logDir/$1.log | mailx -s "Catholic monthly after tax files" $ownerMail
}

state_super_aftertax_data () {
	# (10) State Super monthly after tax files
	echo "State Super monthly after tax files"
	( statesuperAfterTaxData.sh $noSendArg -b $beginDate -e $prevDay 2>&1 ) | tee $logDir/$1.log | mailx -s "State Super monthly after tax files" $ownerMail
}

cooper_aftertax_data () {
	# (11) Cooper monthly after tax files
	echo "Cooper monthly after tax files"
	( cooperAfterTaxData.sh $noSendArg -b $beginDate -e $prevDay 2>&1 ) | tee $logDir/$1.log | mailx -s "Cooper monthly after tax files" $ownerMail
}

bnp_monthly_div_yield () {
	# (12) BNP Paribas Franked Index Data
	echo "BNP Paribas Franked Index Data"
	( bnpMonthlyDivYield.sh $noSendArg -d $prevDay 2>&1 ) | tee $logDir/$1.log | mailx -s "BNP Paribas Div Yield" $ownerMail
}

challenger_frank_percent () {
	# (13) Challenger franking percentage data
	echo "Challenger franking percentage data for I-200"
	( challengerFrankPercent.sh $noSendArg -i I-200 -c XJO 2>&1 ) | tee $logDir/challengerFrankPercent.log | mailx -s "Challenger franking percentage data for I-200" $ownerMail

	echo "Challenger franking percentage data for B-JMPT"
	( challengerFrankPercent.sh $noSendArg -i B-JMPT -c XJMPT 2>&1 ) | tee $logDir/challengerFrankPercent-B-JMPT.log | mailx -s "Challenger franking percentage data for B-JMPT" $ownerMail

	echo "Challenger franking percentage data for XKO"
	( challengerFrankPercent.sh $noSendArg -i I-300 -c XKO 2>&1 ) | tee $logDir/challengerFrankPercent-XKO.log | mailx -s "Challenger franking percentage data for XKO" $ownerMail
	cat $logDir/challengerFrankPercent*.log >  $logDir/$1.log
}

cbus_monthly_aftertax_files () {
	# (14) CBUS monthly after tax files
	echo "CBUS monthly after tax files"
	cbusAfterTaxData.sh $noSendArg -b $beginDate -e $prevDay > $logDir/$1.log 2>&1
}

defect_check () {
# following line should be the last line of this script
# because it checks for potential errors in the output
# produced by all the jobs
	# egrep -i "no|err|fail|ille|unab|segm|warn|trace|core|denied|get_old_prices|timed out|time out|abort|deadlock|Duplicate entries in|can't|don't|must be > 0|test" $logDir/* | egrep -vi 'keyboard|nov|extra|B-FTAU|Translists|DbaseGetCashPosition|cannonical|Fill|Ooh, err,|E-SNO|E-PNO|E-BNO|E-GDNO|E-BNOOA|E-MANO|E-CNNOA|E-MMNO|E-ANO|E-GYNO|E-GPNOA|E-BNOOB|E-TJNO|E-ERNO|E-LINO|E-WTNO|E-HZNO|E-NOD|E-ATNOA|E-PNOOA|E-BWNO|E-BCNO|E-AHNO|E-CLNO|E-PUNO|E-TXNO|E-SNO|E-PNO|E-BNO|E-GDNO|E-BNOOA|E-MANO|E-CNNOA|E-MMNO|E-ANO|E-GYNO|E-GPNOA|E-BNOOB|E-TJNO|E-ERNO|E-LINO|E-WTNO|E-HZNO|E-NOD|E-ATNOA|E-PNOOA|E-BWNO|E-BCNO|E-AHNO|E-CLNO|E-PUNO|E-TXNO|E-XENO|native python|DeprecationWarning|deferredDelivery| = pack\(|No of Trades|Tolhurst|Merrill|Reynolds|No changes|NOD|nonibWeeklyData.py|B-JQUIT|-nonreinvested|Successfully transferred' >> /tmp/errors.monthlyJobs
	if [ `wc -l /tmp/errors.monthlyJobs | awk '{print $1}'` -ne 0 ]; then
		warn "Potential errors exist in monthly jobs"
		cat /tmp/errors.monthlyJobs | mailx -s "monthlyJobs: potential errors from log directory" $ownerMail
	fi	
}
case $task in
    1) logger 'rimes_frank_data' rimes_frank_data;;
    2) logger 'pp_monthly_index_return' pp_monthly_index_return;;
    3) logger 'bt_monthly_gics_changes' bt_monthly_gics_changes;;
    4) logger 'perennial_monthly' perennial_monthly;;
    5) logger 'wilson_weights_returns' wilson_weights_returns;;
    6) logger 'unisuper_monthly' unisuper_monthly;;
    7) logger 'sun_super_aftertax_data' sun_super_aftertax_data;;
	8) logger 'perpetual_aftertax_data' perpetual_aftertax_data;;
	9) logger 'catholic_aftertax_data' catholic_aftertax_data;;
	10) logger 'state_super_aftertax_data' state_super_aftertax_data;;
	11) logger 'cooper_aftertax_data' cooper_aftertax_data;;
	12) logger 'bnp_monthly_div_yield' bnp_monthly_div_yield;;
	13) logger 'challenger_frank_percent' challenger_frank_percent;;
	14) logger 'cbus_monthly_aftertax_files' cbus_monthly_aftertax_files;;
    *) echo "Curret task num: $task"; 
esac

if [ $task -eq 0 ]; then
	logger 'rimes_frank_data' rimes_frank_data
	logger 'pp_monthly_index_return' pp_monthly_index_return
	logger 'bt_monthly_gics_changes' bt_monthly_gics_changes
	logger 'perennial_monthly' perennial_monthly
	logger 'wilson_weights_returns' wilson_weights_returns
	logger 'unisuper_monthly' unisuper_monthly
	logger 'sun_super_aftertax_data' sun_super_aftertax_data
	logger 'perpetual_aftertax_data' perpetual_aftertax_data
	logger 'catholic_aftertax_data' catholic_aftertax_data
	logger 'state_super_aftertax_data' state_super_aftertax_data
	logger 'cooper_aftertax_data' cooper_aftertax_data
	logger 'bnp_monthly_div_yield' bnp_monthly_div_yield
	logger 'challenger_frank_percent' challenger_frank_percent
	logger 'cbus_monthly_aftertax_files' cbus_monthly_aftertax_files
    # logger 'defect_check' defect_check
fi

defect_check


echo "monthly jobs done"

