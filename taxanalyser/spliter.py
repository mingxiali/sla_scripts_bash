#!/usr/bin/env python3

# This script will split the input spreadsheet to three different files:
# i.e. security code, security value and tax lots

# Will generate the files in local folder and will be deployed to tax analyser machine
import os
import argparse
from loguru import logger
import pandas as pd
from csv import QUOTE_ALL


class InputConverter:
    def __init__(self, fn, output_folder):
        self.fn = fn
        self.output_folder = output_folder
        self.data = pd.read_excel(self.fn, sheet_name=0, engine='openpyxl')
        logger.info('Input converter initialized for Vanguard')

    def reformat(self):
        # find the header
        header_row = (self.data.iloc[:, 0].values == 'Portfolio Code').argmax()
        self.data = pd.read_excel(self.fn, sheet_name=0, header=header_row+1, engine='openpyxl')
        self.data = self.data[pd.notnull(self.data['Security Code'])]
        # populate security id
        df_sec = pd.DataFrame(data={'Security Code': self.data['Security Code'].unique()})
        df_sec.insert(0, 'securityid', df_sec.index + 1)
        self.data = pd.merge(self.data, df_sec, how='left', on=['Security Code'])
        logger.info('Input data reformatted')

    def get_security_code(self):
        """ id | securityid | provider | startdate | enddate  | code | company |   issued   |     isin     |
         sedol | underlyingcode | securitydescription | securityabbreviateddescription |
          securityshortdescription | securitytype | firstactivedate | lastactivedate |
           firstquoteddate | lastquoteddate | suspensionstartdate | suspensionenddate"""
        logger.info('Extracting security code data')
        START_DATE = 19700101
        cols = ['securityid', 'Valuation Date', 'Security Code', 'Security Name']
        df_sec = self.data[cols]
        df_sec = df_sec.rename(columns={
            'Valuation Date': 'enddate',
            'Security Code': 'code',
            'Security Name': 'company'
        })
        df_sec.loc[:, 'enddate'] = df_sec['enddate'].apply(lambda x: x.strftime('%Y%m%d')).astype(int)
        # fill in constants
        df_sec = df_sec.drop_duplicates(subset=['enddate', 'code'], keep='first')
        df_sec.insert(1, 'startdate', START_DATE)
        df_sec.insert(1, 'provider', 'asx')
        # generate id columns
        df_sec.insert(0, 'id', df_sec['securityid'])
        # generate other columns
        df_sec.insert(len(df_sec.columns), 'issued', None)
        df_sec.insert(len(df_sec.columns), 'isin', None)
        df_sec.insert(len(df_sec.columns), 'sedol', None)
        df_sec.insert(len(df_sec.columns), 'underlyingcode', None)
        df_sec.insert(len(df_sec.columns), 'securitydescription', None)
        df_sec.insert(len(df_sec.columns), 'securityabbreviateddescription', None)
        df_sec.insert(len(df_sec.columns), 'securityshortdescription', None)
        df_sec.insert(len(df_sec.columns), 'securitytype', 1)
        df_sec.insert(len(df_sec.columns), 'firstactivedate', None)
        df_sec.insert(len(df_sec.columns), 'lastactivedate', None)
        df_sec.insert(len(df_sec.columns), 'firstquoteddate', None)
        df_sec.insert(len(df_sec.columns), 'lastquoteddate', None)
        df_sec.insert(len(df_sec.columns), 'suspensionstartdate', None)
        df_sec.insert(len(df_sec.columns), 'suspensionenddate', None)
        df_sec.to_csv(os.path.join(self.output_folder, 'security_code.csv'), index=False)

    def get_security_value(self):
        """        id         | securityid | provider | effectivedate |     openvalue     |    closevalue     |
             highvalue     |     lowvalue      | pricereturn | totalreturn | marketcap"""
        logger.info('Extracting data for security value')
        cols = ['securityid', 'Valuation Date', 'Units Held', 'Market Value']
        df_val = self.data[cols]
        df_val = df_val.rename(columns={
            'Valuation Date': 'effectivedate',
        })
        df_val['effectivedate'] = df_val['effectivedate'].apply(lambda x: x.strftime('%Y%m%d')).astype(int)
        # generate price
        df_val.insert(len(df_val.columns), 'openvalue', df_val['Market Value']/df_val['Units Held'])
        df_val = df_val.drop_duplicates(subset=['securityid', 'effectivedate'], keep='first')
        df_val = df_val[['securityid', 'effectivedate', 'openvalue']]
        df_val.insert(len(df_val.columns), 'closevalue', df_val['openvalue'])
        df_val.insert(len(df_val.columns), 'highvalue', df_val['openvalue'])
        df_val.insert(len(df_val.columns), 'lowvalue', df_val['openvalue'])
        # generate fields
        df_val.insert(len(df_val.columns), 'pricereturn', None)
        df_val.insert(len(df_val.columns), 'totalreturn', None)
        df_val.insert(len(df_val.columns), 'marketcap', None)
        # generate id
        # a.date::bigint*1000000000+300000000+a.security_id
        id_col = (df_val['effectivedate'].astype('int64')*1000000000+300000000) + df_val['securityid']
        df_val.insert(0, 'id', id_col)
        df_val.insert(2, 'provider', 'asx')
        df_val.to_csv(os.path.join(self.output_folder, 'security_value.csv'), index=False)

    def get_tax_lots(self):
        logger.info('Extracting tax lots')
        # "Fund"," TaxGroupID"," ManagerCode"," PfolioID"," SecurityID",
        # " OriginalSecurityCode"," CurrentSecurityCode"," ISIN        ",
        # " AcquisitionDate"," Gr.UnitPrice"," RevisedPrice"," LocalCurrency",
        # " BaseCurrency"," ExchangeRate  "," Brokerage&OtherCosts",
        # "  NumberofShares        "," PurchaseOrSaleIndicator"," PurchaseID ",
        # " ParentPlacementCode"," BrokerID"," OriginalPfolio"
        df_tl = self.data.copy()
        df_tl.insert(0, ' RevisedPrice', df_tl['CGT Cost Base']/df_tl['Units Held'])
        cols = ['Portfolio Code', 'Security Code', 'Purchase Date', ' RevisedPrice', 'Units Held', 'Purchase ID']
        df_tl = df_tl[cols]
        df_tl = df_tl.rename(columns={
            'Portfolio Code': ' PfolioID',
            'Security Code': ' SecurityID',
            'Purchase Date': ' AcquisitionDate',
            'Units Held': '  NumberofShares        ',
            'Purchase ID': ' PurchaseID '
        })
        df_tl.loc[:, ' AcquisitionDate'] = df_tl[' AcquisitionDate'].apply(lambda x: x.strftime('%Y%m%d'))
        # fill in missing fields
        df_tl.insert(0, 'Fund', 'VANGUARD')
        df_tl.insert(1, ' TaxGroupID', 'GBSTS1')
        df_tl.insert(2, ' ManagerCode', None)
        df_tl.insert(5, ' OriginalSecurityCode', None)
        df_tl.insert(6, '  CurrentSecurityCode', None)
        df_tl.insert(7, ' ISIN        ', None)
        df_tl.insert(9, ' Gr.UnitPrice', None)
        df_tl.insert(11, ' LocalCurrency', 'AUD')
        df_tl.insert(12, ' BaseCurrency', 'AUD')
        df_tl.insert(13, ' ExchangeRate  ', '1.0')
        df_tl.insert(14, ' Brokerage&OtherCosts', None)
        df_tl.insert(16, ' PurchaseOrSaleIndicator', None)
        df_tl.insert(18, ' ParentPlacementCode', None)
        df_tl.insert(19, ' BrokerID', None)
        df_tl.insert(20, ' OriginalPfolio', None)
        df_tl.loc[:, ' PurchaseID '] = df_tl[' PurchaseID '].astype(str)
        df_tl.to_csv(os.path.join(self.output_folder, 'tax_lots.csv'), index=False, quoting=QUOTE_ALL)
        logger.info('Please check the files in the output folder')

    def run(self):
        self.reformat()
        self.get_security_code()
        self.get_security_value()
        self.get_tax_lots()


# def main():
#     InputConverter('input/VG_GBST Sample Report.xlsx', 'output').run()
#     pass


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Vanguard input splitter', prog='input splitter')
    parser.add_argument('-i', '--input', type=str, help='abs path of input file', required=True)
    parser.add_argument('-o', '--output', type=str, help='output folder', required=True)
    args = parser.parse_args()
    InputConverter(args.input, args.output).run()

