#!/usr/bin/env python3
import sys
import os
cmd = ' '.join(sys.argv[1:])
program = os.path.basename(__file__)
if program != 'read_config.py':
    program=program.replace('.py', '')
cmd = f'{program} {cmd}'
os.system(f'qds -c "{cmd}"')