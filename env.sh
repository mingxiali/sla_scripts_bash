# install python
sudo apt install -y python2
# install pip
curl https://bootstrap.pypa.io/pip/2.7/get-pip.py --output get-pip.py
# install dev packages
sudo apt-get install libffi-dev
sudo apt install -y gcc
sudo apt-get install python-dev
sudo apt install -y build-essential
sudo apt-cache depends python-pycurl 
sudo apt-get install libcurl4-gnutls-dev
# install packages
pip install -r requirements.txt
# install other packages
# tar xzf pybonjour-1.1.1.tar.gz
# cd pybonjour-1.1.1/
# install bash debugger
cd downloads/
tar xzf bashdb-5.0-1.1.1.tar.gz
cd bashdb-5.0-1.1.1/
./configure
make
sudo make install
# migrate the files
sudo mkdir -p /projects/qds/bin
sudo mkdir -p /projects/common
sudo cp /home/$USER/downloads/read_config.py /projects/qds/bin
# give permission
sudo chown -R $USER .
sudo chmod +x /projects/qds/bin/read_config.py

# modify the .bash* files
cd /home/$USER/downloads/
sudo cp configuration.py `python -m site --user-site`
sudo cp configuration.xml /projects/common

# set environment
sudo echo "export PYTHONPATH=/usr/lib/python2.7" >> ~/.bash_profile
sudo echo "export CONFIGURATION=/projects/common/configuration.xml:au:melbourne" >> ~/.bash_profile
echo "export GBSTPASS=[your_password]" | sudo tee -a ~/.bash_profile 
sudo echo "source ~/.bashrc" >> ~/.bash_profile

# create folder
sudo mkdir -p /data/client/log/morning/
sudo touch /data/client/log/morning/timestamps.log

# git config
 git config --global user.email ming.li@gbst.com
 git config --global user.name $USER

 # fix DNS 

 More recent resolution:

# 1. cd ~/../../etc (go to etc folder in WSL).
# 2. echo "[network]" | sudo tee wsl.conf (Create wsl.conf file and add the first line).
# 3. echo "generateResolvConf = false" | sudo tee -a wsl.conf (Append wsl.conf the next line).
# 4. wsl --terminate Debian (Terminate WSL in Windows cmd, in case is Ubuntu not Debian).
# 5. cd ~/../../etc (go to etc folder in WSL).
# 6. sudo rm -Rf resolv.conf (Delete the resolv.conf file).
# 7. In windows cmd, ps or terminal with the vpn connected do: Get-NetIPInterface or ipconfig /all for get the dns primary and
# secondary.
# 8. With the dns primary and secondary getted replace the numbers in the next step in the X.X.X.X
# 9. echo "nameserver X.X.X.X" | sudo tee resolv.conf (Create resolv.conf and append the line.)
# 10. echo "nameserver X.X.X.X" | sudo tee -a resolv.conf (Append the line in resolv.conf)
# 11. wsl --terminate Debian (Terminate WSL in Windows cmd, in case is Ubuntu not Debian).
# 12. sudo chattr +i resolv.conf
# 13. And finally in windows cmd, ps or terminal:
# Get-NetAdapter | Where-Object {$_.InterfaceDescription -Match "Cisco AnyConnect"} | Set-NetIPInterface -InterfaceMetric 6000
# 
# Credit: @MartinCaccia, @yukosgiti, @machuu and @AlbesK:
# https://github.com/microsoft/WSL/issues/4277
# https://github.com/microsoft/WSL/issues/4246# 

# sudo chmod 744 /etc/resolv.conf
# sudo chmod 644 /etc/resolv.conf
# sudo chattr +i /etc/resolv.conf
# sudo chattr -i /etc/resolv.conf

# USE wsl1 for linux dist in PS
# wsl --set-version Ubuntu-20.04 1
# configure jump machine
# set up go to alias
sudo apt install -y expect
# copy qds_machine to the distro and create alias
echo "alias goto='expect ~/projects/bash_scripts/qds_machine.exp'" | sudo tee -a ~/.bash_profile 
# create 
ssh -t 'command; bash -l'

# https://www.illuminiastudios.com/dev-diaries/ssh-on-windows-subsystem-for-linux/
# ssh set up
sudo apt remove openssh-server
sudo apt install openssh-server

sudo -H pip3 install -U pipenv

# Change PasswordAuthentication to yes
# AllowUsers mingl
sudo service ssh start
sudo visudo
# add the following line %sudo ALL=NOPASSWD: /usr/sbin/sshd after %sudo  ALL=(ALL:ALL) AL

sudo service ssh --full-restart

cat id_rsa.pub | `mkdir -p ~/.ssh && touch ~/.ssh/authorized_keys && chmod -R go= ~/.ssh && cat >> ~/.ssh/authorized_keys`

# ssh to remote

ssh -t mingl@SYDLT2401 'source ~/.bash_profile; expect ~/projects/bash_scripts/qds_machine.exp'

# https://seankilleen.com/2020/04/how-to-create-a-powershell-alias-with-parameters/
# powershell alias
function goto
{
	ssh -t mingl@SYDLT2401 "source ~/.bash_profile; expect ~/projects/bash_scripts/qds_machine.exp $args"
}

# Docker in wsl
# https://nickjanetakis.com/blog/setting-up-docker-for-windows-and-wsl-to-work-flawlessly

# auto restart ssh
# https://gist.github.com/dentechy/de2be62b55cfd234681921d5a8b6be11

sudo echo 'export PIPENV_VENV_IN_PROJECT="enabled"' >> ~/.bash_profile

# connect to //general/mingl
sudo mkdir /mnt/general
sudo mkdir /mnt/client

sudo echo "\\general\mingl /mnt/general drvfs defaults 0 0" > /etc/fstab
sudo echo "//172.19.117.125/client/mingl /mnt/client drvfs defaults 0 0" > /etc/fstab

# sudo mount -t drvfs //172.19.117.125/client/mingl /mnt/client
# sudo mount -t drvfs '\\general\mingl' /mnt/general

sudo echo "alias upload='expect ~/projects/bash_scripts/repo_uploader.exp'" >> ~/.bash_profile

# run postgres container
docker run -p 5432:5432 --name postgres:9.2.13 -e POSTGRES_PASSWORD=$GBSTPASS -d postgres


# config the initial settings in /docker-entrypoint-initdb.d/init-user-db.sh
#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
	CREATE ROLE mingl LOGIN SUPERUSER PASSWORD 'dbpass2021';
    CREATE USER data_writer WITH PASSWORD 'data_writer@';
    CREATE DATABASE event_logger;
    GRANT ALL PRIVILEGES ON DATABASE event_logger TO data_writer;
    CREATE DATABASE unisuper;
    GRANT ALL PRIVILEGES ON DATABASE unisuper TO data_writer;
    CREATE DATABASE commonnew;
    GRANT ALL PRIVILEGES ON DATABASE commonnew TO data_writer;
    GRANT ALL PRIVILEGES ON TABLE events TO data_writer;
EOSQL


# connect to the database with command
psql -h 127.0.0.1 -p 5432 -U data_writer -d event_logger

sudo apt-get install libpq-dev

# mailx
sudo apt-get install bsd-mailx
sudo mkfifo /var/spool/postfix/public/pickup
sudo service postfix restart

# cron job
sudo service cron start

# Add 40 11 * * * ~/projects/bash_scripts/debug_tasks.sh -n -d 20180403 2>&1

# root task at startup
sudo crontab -e
# @reboot mount -t drvfs '\\general\mingl' /mnt/general

sudo apt-get install openjdk-8-jre
sudo apt-get install openjdk-8-jdk

# install python3.6
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt update
sudo apt install python3.6

# install python3.3
sudo wget https://www.python.org/ftp/python/3.3.0/Python-3.5.2.tgz
sudo tar xzf Python-3.3.0.tgz
cd Python-3.3.0
sudo ./configure
sudo make altinstall

# devel2
mkdir -p /data/client/mingl/projects/bash_scripts
mkdir -p /data/client/mingl/projects/event_logger

sudo apt-get install openjdk-11-jre
sudo apt-get install openjdk-11-jdk

# hyper terminal with multiple tabs
# https://medium.com/@ssharizal/hyper-js-oh-my-zsh-as-ubuntu-on-windows-wsl-terminal-8bf577cdbd97

# send command via wsl
# wsl -s Ubuntu-20.04
# wsl ls -la

# fstab exmaple: //10.10.0.2/adam /run/mount/nas/adam cifs rw,noauto,noexec,_netdev,x-systemd.automount,x-systemd.mount-timeout=60,x-systemd.idle-timeout=1min,username=adam,password=PASSWORD,uid=adam,gid=adam,vers=2.0 0 0

# vi ~/.ssh/config
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash
source ~/.bash_profile
nvm install  6.11.3

echo "[[ -s $HOME/.nvm/nvm.sh ]] && . $HOME/.nvm/nvm.sh" | sudo tee -a ~/.bash_profile 


mkdir ~/.npm-global
npm config set prefix '~/.npm-global'
echo "export PATH=~/.npm-global/bin:$PATH" | sudo tee -a ~/.bash_profile 
source ~/.bash_profile

