#!/bin/sh
name=`ps -aef | grep [w]araRunDaily |sed 's/.*-i //' | cut -d " " -f1`
if [ -z "$name" -a ]; then echo "No process running" && exit 1; fi
sum=`wc -l /data/aftertax/warakirri/benchmark.list  | cut -d " " -f6`
pos=`grep -n $name /data/aftertax/warakirri/benchmark.list | cut -d: -f1`
echo "Current: $name in progress; $pos out of $sum processed"