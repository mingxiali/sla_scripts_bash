#!/usr/bin/env python3
import os
import pandas as pd
import paramiko
import time
from loguru import logger
from paramiko.auth_handler import SSHException
from paramiko_expect import SSHClientInteraction
from definitions import config, ROOT_DIR
import shutil


class Shell:
    """Client to interact with a remote host via SSH & SCP."""
    def __init__(self, machine=None, delay=0.2, login_user='qdsclient', reason='work'):
        self.login_user = login_user
        if machine is None:
            self.machine = f'{login_user}@pds-proc2'
        else:
            self.machine = f'{login_user}@{machine}'
        self.user = config.get('SSH', 'UserName')
        self.host = config.get('SSH', 'Host')
        self.pwd = config.get('Credentials', 'Password')
        self.reason = reason
        self.delay = delay
        # set up channel 1 to interact and channel 2 to check status
        self.client = paramiko.SSHClient()
        self.client.load_system_host_keys()
        self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.client.connect(self.host, username=self.user, password=self.pwd)
        # set up dl channel
        self.dl_channel = self.client.invoke_shell()
        self.channel = self.login(self.client)
        self.output_folder = '~/term/'
        self.channel.send(f'[ ! -d ~/term ] && mkdir -p {self.output_folder}\n')

    def login(self, client):
        interact = SSHClientInteraction(client, timeout=10, display=False)
        assert interact.expect(f'{self.user}\$\s', timeout=15 * self.delay) == 0
        interact.send(f'xtn {self.machine}')
        assert interact.expect('.*access:\s', timeout=15 * self.delay) == 0
        interact.send(self.reason)
        assert interact.expect('.*Password:\s', timeout=15 * self.delay) == 0
        interact.send(self.pwd)
        assert interact.expect(f'{self.login_user}\$\s', timeout=5 * self.delay) == 0
        return interact

    def block_send(self, cmd, download=False, cp_to_local=True):
        # self.check_active()
        dt = pd.Timestamp.now().strftime('%m%d%Y%H%m%S')
        fn = f'task_{dt}.txt'
        postfix = f'; touch {self.output_folder}{fn}'
        if cmd.endswith('\n'):
            cmd = cmd.rstrip()
        if cmd.endswith(';'):
            cmd = cmd[:-1]
        # check kw in receive
        if 'sh -c' in cmd and cmd[-1] == "'":
            s_cmd = cmd[:-1] + postfix + "'"
            self.channel.send(s_cmd)
        else:
            self.channel.send(cmd + postfix)
        logger.info(f'Running command $"{cmd}" on {self.machine}')
        assert self.channel.expect(f'{self.login_user}\$\s', timeout=5 * self.delay) == 0
        if download:
            # use xtn and check file in download folder
            # make sure the process is complete
            # eg: xtn qdsclient@qds-devel2:/home/qdsclient/term/task_06212021130623.txt /home/mingl/tasks
            dl_cmd = f'xtn {self.machine}:/home/{self.login_user}/term/{download} /home/{self.user}/downloads && touch /home/mingl/tasks/{fn}'
            self.dl_channel.send(dl_cmd + '\n')
            # give reason and password
            time.sleep(self.delay)
            self.dl_channel.send(f'{self.reason}\n')
            time.sleep(self.delay)
            self.dl_channel.send(self.pwd + '\n')
            while True:
                if os.path.exists(f'\\\\general\\mingl\\tasks\\{fn}'):
                    logger.info(f'File {download} fetched to general downloads folder')
                    if cp_to_local:
                        shutil.copy2(f'\\\\general\\mingl\\downloads\\{download}', os.path.join(ROOT_DIR, 'shell', 'files'))
                    break
                time.sleep(self.delay)

    def upload(self, file, des=None):
        # self.check_active()
        if des is None:
            des = f'/home/{self.login_user}/term'
        # copy to uploads
        dt = pd.Timestamp.now().strftime('%m%d%Y%H%m%S')
        fn = f'task_{dt}.txt'
        logger.info('Copying files to upload folder')
        shutil.copy2(file, f'\\\\general\\mingl\\uploads\\{os.path.basename(file)}')
        # xtn to des
        ul_cmd = f'xtn /home/{self.user}/uploads/{os.path.basename(file)} {self.machine}:{des}  && touch /home/mingl/tasks/{fn}'
        self.dl_channel.send(ul_cmd + '\n')
        # give reason and password
        time.sleep(self.delay)
        self.dl_channel.send(f'{self.reason}\n')
        time.sleep(self.delay)
        self.dl_channel.send(self.pwd + '\n')
        logger.info(f'Uploading file {file} to {des}')
        while True:
            if os.path.exists(f'\\\\general\\mingl\\tasks\\{fn}'):
                logger.info(f'File {file} uploaded to {des} folder')
                break
            time.sleep(self.delay)

    def __del__(self):
        try:
            self.client.close()
        except SSHException as error:
            logger.info('Client shutdown failed!')
            logger.error(error)
            raise error


if __name__ == '__main__':
    # Tests
    # for 1000 times, how many times does the program login successfully
    shell = Shell(machine='qds-devel2')
    shell.block_send('echo first;sleep 10;echo second')
    shell.upload(os.path.join(ROOT_DIR, 'shell', 'files', 'a.csv'))
