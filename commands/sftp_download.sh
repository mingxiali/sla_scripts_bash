#!/bin/sh

today=`date +%Y%m%d`
local_dir=`read_config.py -s msci:local_dir`
remote_host=`read_config.py -s msci:remote_host`
remote_dir=`read_config.py -s msci:remote_dir`
user=`read_config.py -s msci:username`
# pass=`read_config.py -s msci:password`

cd /data/common/test_msci

echo "mget 20210920us_ace.zip" > batch.script
# echo "AGHy2b75DvrLUC" | sftp -b batch.script -o ProxyCommand="/usr/lib/ssh/ssh-http-proxy-connect -h ci-hs-util -p 8080 sftp.msci.com 22" ${user}@${remote_host}
sftp -b batch.script  -o IdentityFile=/home/qdsload/.ssh/qds_msci -o ProxyCommand="/usr/lib/ssh/ssh-http-proxy-connect -h ci-hs-util -p 8080 sftp.msci.com 22" ${user}@${remote_host} << HERE
mget 20210920us_ace.zip
HERE

# check file
