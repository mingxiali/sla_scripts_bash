#! /usr/bin/sh
Usage="Usage: $0 [-s start] [-e end] [-f ftp] [index_list]"
# set up defaults
ftp=0
index=0

echo "Please check if need to run in no send mode first"

while getopts "s:e:f" c ; do
        case $c in
            s) start=$OPTARG;;
            e) end=$OPTARG;;
            f) ftp=1;;
            *) echo "$Usage";
            exit 1;;
        esac
done

shift $((OPTIND-1))
index=$@

if [ -z $index ]; then
  echo "Looping over all benchmarks"
fi

# run calcs
d=$start
while [ $d -le $end ]
do
        # echo ${d}
        if [ $ftp -eq 1 ]; then
                echo "Uploading files for ${d}"
                cd /data/aftertax/warakirri/output
                ls -ltr cash_float_${d}.csv
                ls -ltr ${d}.csv
                ls -ltr ${d}_out.csv
                ls -ltr tax_lots_${d}.csv
                expect /data/client/mingl/projects/bash_scripts/wara-ftp.exp /data/aftertax/warakirri/output cash_float_${d}.csv
                expect /data/client/mingl/projects/bash_scripts/wara-ftp.exp /data/aftertax/warakirri/output ${d}.csv
                expect /data/client/mingl/projects/bash_scripts/wara-ftp.exp /data/aftertax/warakirri/output ${d}_out.csv
                expect /data/client/mingl/projects/bash_scripts/wara-ftp.exp /data/aftertax/warakirri/output tax_lots_${d}.csv
        else
                if [ -z $index ]; then
                        echo "Running calcs for all benchmarks on $d" 
                        # (CONFIGURATION=/projects/common/configuration.xml:au:melbourne /projects/aftertax/bin/waraBatchRunNew.sh -d $d >/data/aftertax/warakirri/DUMP_$d 2>&1);    egrep -i "no|err|fail|illegal|una|seg|warn|trace|almost full|can''t" /data/aftertax/warakirri/DUMP_$d | egrep -v "Ooh, err|selling stocks no longer in index|keyboard|native python datetime will|DeprecationWarning: native python datetime|^\+|^indices=|^nontrading_file|\-n does not send data to clients|\-n: noUpdate               = False|C do NOT clean up" | /usr/bin/mailx -s "ATB - Wara - potential errors for $d"  ming.li@gbst.com
                else
                        for idx in $index
                        do 
                                echo "Running calcs for $idx on $d"
                                (CONFIGURATION=/projects/common/configuration.xml:au:melbourne /projects/aftertax/bin/waraBatchRunNew.sh -d $d -I $idx >/data/aftertax/warakirri/DUMP_$d 2>&1);    egrep -i "no|err|fail|illegal|una|seg|warn|trace|almost full|can''t" /data/aftertax/warakirri/DUMP_$d | egrep -v "Ooh, err|selling stocks no longer in index|keyboard|native python datetime will|DeprecationWarning: native python datetime|^\+|^indices=|^nontrading_file|\-n does not send data to clients|\-n: noUpdate               = False|C do NOT clean up" | /usr/bin/mailx -s "ATB - Wara - potential errors for $idx on  $d" ming.li@gbst.com
                        done
                fi
        fi
        d=`dateformat -N $d`
done
