#!/usr/bin/env python3
import getopt
import sys
import pandas as pd
  
# Usage string
usage = """Usage:
dateformat [-iIseac] [-PN | -pn | -D Days | -W Weeks | -M Months | -Y Years] 
[Date ...]
        -P to output previous weekday of Date
        -N to output next weekday of Date
        -p to output yesterday of Date
        -n to output tomorrow of Date
           (You can use -Pp &/or -Nn multiple times.)
        -D to output the date which is Days calendar days different from Date        -W to output the date which is Weeks weeks different from Date       
        -M to output the date which is Months months different from Date     
        -Y to output the date which is Years years different from Date       
           if no Date specified then default is today
        -i to output in YYYYMMDD
        -I to output in DDMMYYYY
        -s to output in DD/MM/YYYY
        -e to output in YY-MM-DD (note: YY is last 2 digits of current year) 
   -d to output in YYYY-MM-DD
        -a to output in YYMMDD (note: YY is last 2 digits of current year)   
        -c to output as "Sun Apr 24, 1992"
        -m to output as "Apr 24 1992"
        -r to output as "24 Apr 1992" (RFC2822)
           default is -i
"""
argv = sys.argv[1:]

if len(argv) and not argv[-1].startswith('-'):
    in_date = pd.to_datetime(argv[-1])
else:
    in_date = pd.to_datetime('today')

try:
    opts, args = getopt.getopt(argv, "f:l:iP:M:N:ms")
except Exception as e:
    print(e)

output_fmt = '%Y%m%d'
date_str = ""

for index, item in enumerate(opts):
    opt, arg = item
    if opt in ['-i']:
        date_str = in_date.strftime(output_fmt)
        break
    elif opt in ['-P']:
        if arg == 'N':
            # previous bday start of curent month
            month_start = pd.to_datetime(in_date.strftime('%Y%m'), format='%Y%m')
            bmonth_start = pd.tseries.offsets.BMonthBegin().rollforward(month_start)
            date_str = bmonth_start.strftime(output_fmt)
        else:
            date_str = (in_date - pd.tseries.offsets.BDay(1)).strftime(output_fmt)
        break
    elif opt in ['-N']:
        date_str = (in_date + pd.tseries.offsets.BDay(1)).strftime(output_fmt)
        break
    elif opt in ['-M']:
        month_start = pd.to_datetime(in_date.strftime('%Y%m'), format='%Y%m')
        date_str = (month_start + pd.DateOffset(months=int(arg))).strftime(output_fmt)
        break
    elif opt in ['-m']:
        output_fmt = '%b %d %Y'
    elif opt in ['-s']:
        output_fmt = '%d/%m/%Y'
print(date_str)
