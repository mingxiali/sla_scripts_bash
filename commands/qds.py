#!/usr/bin/env python3
# Prerequests: deploy jr command line in the path; Set up VPN connection
import pexpect
import os
import subprocess
import argparse

# env variables
pwd = os.environ.get('GBSTPASS')
user = os.environ.get('USER')

parser = argparse.ArgumentParser(description='qds machine agent', prog='qds')
parser.add_argument('-u', '--user', type=str, choices=['qdsclient', "qdsbuild", "qdsbatch", "jboss", "qdsload"], help='user', default='qdsclient')
parser.add_argument('-m', '--machine', type=str, choices=['proc2', "devel2", "taxanalyser-prod-2", "taxanalyser-dr-2"], help='machine', default='proc2')
parser.add_argument('-c', '--command', type=str, help='command', default=None)
parser.add_argument('-t', '--ticket', type=str, help='ticket', default=None)
# TODO: add job commands
parser.add_argument('-j', '--job', type=str, help='run job command on remote machine', choices=['solaris rerun', 'warakirri rerun', 'wp'])
# TODO: tranfer files

args = parser.parse_args()

# take parameters
login_user = args.user
machine = f"qds-{args.machine}"
# cmd = "dateformat -i"
cmd = args.command

if args.job == 'wp':
    cmd = 'sh /data/client/mingl/scripts/wp.sh'

if args.ticket is None:
    ticket = subprocess.check_output('jr').strip()
else:
    ticket = args.ticket

PROMPT = [f'\r\n{user}\$ ', f'\r\n{login_user}\$ ']
child = pexpect.spawn (f'ssh general.gbst.net')
child.expect ('Password:', timeout=10)
child.sendline (pwd)
child.expect (PROMPT, timeout=10)
child.sendline(f"xtn {login_user}@{machine}")
child.expect("Reason for access:", timeout=10)
child.sendline(ticket)
child.expect("Password: ", timeout=10)
child.sendline(pwd)
child.expect(PROMPT, timeout=5)
if args.command is None:
    greetings = child.before.decode('utf-8').split('\r\n')[-1]
    print(greetings)
    child.interact()
else:
    output = child.before
    child.sendline(cmd)
    child.expect (PROMPT, timeout=10)
    # post buffer
    output = child.before.decode('utf-8')
    # trim the last $machine:/
    output = ''.join(output.split(f'{machine}:/')[:-1]).strip()
    # trime the datetime
    output = '\r\n'.join(output.split('\r\n')[:-1])
    # trim the begining command
    if output.startswith(cmd):
        output = output[len(cmd):].strip()
    print(output)