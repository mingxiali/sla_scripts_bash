#!/usr/bin/env python3
"""
fetch result from proc2 and echo to stdout
"""
import sys 
import os

sys.argv[0] = os.path.basename(sys.argv[0])

cmd = ' '.join(sys.argv)

# send command to proc2 and echo return

print(cmd)