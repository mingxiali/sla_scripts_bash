#!/usr/bin/env python3

import os

deploys = [('jira_cli.py', 'jr'), ('qds.py', 'qds')]

for src, target in deploys:
    cmd = f"~/projects/bash_scripts/commands/{src} ~/bin/{target}"
    os.system(cmd)

deploys = ['instr', 'histrep', 'dateformat']

for dep in deploys:
    cmd = f"~/projects/bash_scripts/shortcuts/sc.py ~/bin/{dep}"
    os.system(cmd)