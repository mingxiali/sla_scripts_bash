#! /bin/sh
#----------------------------------------------------------------------------
# Creates the entries in the atb_ tables for a given date range and index
# Extracts them from the database and copy to FTP site
#----------------------------------------------------------------------------
# $Header: /import/cvs/aftertax/scripts/waraBatchRunNew.sh,v 1.5 2018/05/08 01:35:42 qdsbuild Exp $
#----------------------------------------------------------------------------

PATH=/usr/bin:/usr/local/bin:/usr/sfw/bin:/projects/qds/bin:/projects/client/bin:/projects/aftertax/bin
export PATH
PERLLIB=$PERLLIB:/projects/perl5
export PERLLIB
. /opt/sybase_15_0_3_OC/SYBASE.sh

commonDir=`read_config.py -s common:dir`

# Source QDS common script first
. ${commonDir}/utility.sh

dateToUse=`dateformat -i today`
dontSendData=0
theSingleBenchmark=""
isSingleBenchmark=""
dontCleanUp=0
dbname=`read_config.py -s "database:quant"`
host=`read_config.py -s "database:server"`
wuser=`read_config.py -s "database:wuser"`
wpasswd=`read_config.py -s "database:wpassword"`


Usage="
Usage: $0 [-d date] [-I the single benchmark] [-n] [-C]
Calulates after tax files for Warakirri
-d date: default is $dateToUse
-I the single benchmark: default is $theSingleBenchmark
-n does not send data to clients
-C do NOT clean up: -C should be included for every in-specie transactions (clean up should be run separately)
"

while getopts d:nI:C x
do
    case $x in
        d) dateToUse=`dateformat -i $OPTARG`;;
        I) theSingleBenchmark=`instr -c $OPTARG`; if [ "${theSingleBenchmark}" = "" ]; then echo "ERROR: Invalid single benchmark $OPTARG"; exit 1; fi;;
        n) dontSendData=1;;
        C) dontCleanUp=1;;
        \?) echo "$Usage"
            exit 1;;
    esac
done


baseDir=`read_config.py -s aftertax:data_dir`
BBFile=${baseDir}/warakirri/buyBackFile
baseDataDir=${baseDir}/warakirri
cd_or_die $baseDataDir


# Check ratio, cash and data flags

flagfileDir=${baseDir}/flagfiles
ratioFlag=${flagfileDir}/${dateToUse}.Ratio
cashFlag=${flagfileDir}/${dateToUse}.Cash.Wara
dataFlag=${flagfileDir}/${dateToUse}.Data.Confirm

if [ ! -r ${ratioFlag} ] ; then
    echo "ERROR: ratio flag not set. ${ratioFlag}"
    exit 1
fi
if [ ! -r ${cashFlag} ] ; then
    echo "ERROR: cash flag not set. ${cashFlag}"
    exit 1
fi
if [ ! -r ${dataFlag} ] ; then
    echo "ERROR: data flag not set. ${dataFlag}"
    exit 1
fi


if [ ! -f Cashflows/Cashflows_${dateToUse}.csv ] ; then
    echo "ERROR: Cashflows_${dateToUse}.csv file does not exist"
    exit 1
fi

if [ ! -f buyBackFile ] ; then
    echo "ERROR: buyBackFile${dateToUse}.csv file does not exist"
    exit 1
fi

# Add separator as needed for single benchmark runs
if [ "$theSingleBenchmark" != "" ]; then
    isSingleBenchmark="_"
fi

cd_or_die ${baseDataDir}

# Read each line in the benchmark.list. Format is:
# benchmark index [active Y/N] [methodology]
# where methodology in: 0 = FIFO, 1 = LOW_GAIN, 2 = HIFO, 3 = HC_12M, 4 = HIGH_GAIN

while read line; do
    # If column 3 = N then skip
    # 
    if [ `echo $line | awk '{print $3}'` = "N" ]; then
        continue
    else
        if [ `echo $line | awk '{print NF}'` -ne 4 ]; then
            echo "ERROR: missing methodology: $line"
            exit 1
        fi

        index=`echo $line | awk '{print $1}'`
        indexId=`instr -i -d $dateToUse \`echo $line | awk '{print $1}'\``
        baseIndex=`echo $line | awk '{print $2}'`
        methodology=`echo $line | awk '{print $4}'`

        # Run processing
        if [ "$theSingleBenchmark" = "" -o "$theSingleBenchmark" = `instr -c $index` ]; then
            echo
            echo "******"
            echo "****** Processing index $index, indexId = $indexId, baseIndex $baseIndex, methodology $methodology ******"
            echo "******"
            echo

            cd_or_die $index
            if [ ! -d gr ]; then
                mkdir gr
            fi
            cd_or_die $baseDataDir


            prevDate=`dateformat -P $dateToUse`

            cd_or_die ${baseDataDir}/output
            if [ "$theSingleBenchmark" = "" ]; then
                outFile.py -d $dateToUse -i $indexId -B $BBFile | grep "^$dateToUse" >> $dateToUse.csv
            else
                outFile.py -d $dateToUse -i $indexId -B $BBFile | grep "^$dateToUse" > ${theSingleBenchmark}${isSingleBenchmark}$dateToUse.csv
            fi  

            # Drop back down a directory
            cd_or_die ${baseDataDir}
        fi
    fi
done < /data/aftertax/warakirri/benchmark.list

cd_or_die ${baseDataDir}

