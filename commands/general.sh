#! /usr/bin/expect
log_user 0

set timeout 500;

spawn ssh general.gbst.net
expect_before timeout abort
expect -exact "\rPassword: "
send "$::env(GBSTPASS)\n"
interact