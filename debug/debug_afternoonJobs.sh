#!/bin/sh
# $Header: /import/cvs/client/bin/afternoonJobs.sh,v 1.48 2020/03/03 08:07:17 qdsbuild Exp $

PATH=/usr/bin:/usr/local/bin:/usr/sfw/bin:/projects/qds/bin:/projects/client/bin:/projects/aftertax/bin
export PATH
PERLLIB=$PERLLIB:/projects/perl5
export PERLLIB
. /opt/sybase_15_0_3_OC/SYBASE.sh

# Source QDS common script first
common_dir=`read_config.py -s common:dir`
. $common_dir/utility.sh

today=`dateformat -i`
sendMail=1
force=0
ownerMail=`read_config.py -s client:alertmail`
baseDir=`read_config.py -s client:data_dir`
logDir=`read_config.py -s client:log_dir`
logDir=$logDir/4pm
task=0
### TODO: change it to normal mode
debugMode=1
if [ $debugMode -eq 1 ]; then 
    ### DEBUG ###
    ownerMail='ming.li@gbst.com'
    ### DEBUG END ###
fi
echo log directory is $logDir

# log file to capture timing info
timeLog=$logDir/timestamps.log
rm -f $timeLog

rm /tmp/errors.afternoonJobs

timestamp () {
	stamp=`date '+%Y-%m-%d %H:%M:%S'`
	echo "$@: $stamp" >> $timeLog
}

# takes name and function
logger () {
        timestamp "$1 - START"
        ($2 $1)
        # log errors
        if [[ -f "$logDir/$1.log" ]]; then
            egrep -i "no|err|fail|ille|unab|segm|warn|trace|core|denied|get_old_prices|timed out|time out|abort|deadlock|Duplicate entries in|can't|don't|must be > 0|test" $logDir/$1.log | egrep -vi 'keyboard|nov|extra|B-FTAU|Translists|DbaseGetCashPosition|cannonical|Fill|Ooh, err,|E-SNO|E-PNO|E-BNO|E-GDNO|E-BNOOA|E-MANO|E-CNNOA|E-MMNO|E-ANO|E-GYNO|E-GPNOA|E-BNOOB|E-TJNO|E-ERNO|E-LINO|E-WTNO|E-HZNO|E-NOD|E-ATNOA|E-PNOOA|E-BWNO|E-BCNO|E-AHNO|E-CLNO|E-PUNO|E-TXNO|E-SNO|E-PNO|E-BNO|E-GDNO|E-BNOOA|E-MANO|E-CNNOA|E-MMNO|E-ANO|E-GYNO|E-GPNOA|E-BNOOB|E-TJNO|E-ERNO|E-LINO|E-WTNO|E-HZNO|E-NOD|E-ATNOA|E-PNOOA|E-BWNO|E-BCNO|E-AHNO|E-CLNO|E-PUNO|E-TXNO|E-XENO|native python|DeprecationWarning|deferredDelivery| = pack\(|No of Trades|Tolhurst|Merrill|Reynolds|No changes|NOD|nonibWeeklyData.py|B-JQUIT|\[additional.txt\]|no data to send' > /tmp/errors.afternoonJobs.$1.log
            cat /tmp/errors.afternoonJobs.$1.log >> /tmp/errors.afternoonJobs
        fi
        ret_code=$?
        # if return code found, write error message
        if [ $ret_code -ne 0 ]; then
            echo "ERROR exit code found for $1"
            echo  "ERROR: exit code $ret_code for task: $1" >> /tmp/errors.afternoonJobs.$1.log
        fi
        timestamp "$1 - END"
}

Usage="Usage: $0 [-d date] [-n] [-m address]
    -d date: Defaults to current date 
    -n: Don't send data emails to clients (internal status mail are still sent)
	-m address: Internal status mail address (default to $ownerMail)
    -F: Force run
    -t : default as zero to run all tasks
        1) gics_perpetual
        2) bt_back_office_load_np_split
        3) cfs_daily_gics
        4) cfs_data' cfs_data
        5) cc_data_decommision
        6) esuper_fund_eod_prices
        7) atb_data_flag
"

while getopts "t:m:d:n:F" c ; do
    case $c in
    t) task=$OPTARG;;
    m) ownerMail=$OPTARG;;
	d) today=$OPTARG;;
    n) sendMail=0;;

    F) force=1;;
    *) echo "$Usage";
        exit 1;;
    esac
done

noSendArg=""

if [ $sendMail -eq 0 ]; then
    noSendArg="-n"
fi

tomorrow=`dateformat -N $today`
Dow=`dateformat -c $today | awk '{print $1};'`

# Sat-Sun check
if [ $Dow = "Sat" -o $Dow = "Sun" ]; then
    exit 1
fi

# Wait for afternoon processing flagfile
flagfile_dir="`read_config.py -s dbaseload_dir`/data/flagfiles"
die_time=1700
if [ `is_halftrading ${today}` -eq 1 ]; then
    die_time=1500
fi
if [ $debugMode -eq 0 ]; then
    if [ `dateformat -i` -eq $today ] ; then
        while [ ! -r $flagfile_dir/$today.4pm -a `date +%H%M` -lt $die_time ] 
        do
            info "INFO: Waiting for afternoon processing flagfile at `date`"
            sleep 60
        done
        if [ ! -r $flagfile_dir/$today.4pm -a `date +%H%M` -ge $die_time ] ; then 
            die "ERROR: Afternoon processing failed."
        fi
    fi
fi

gics_perpetual () {

# (1) GICS for perpetual for rothschild/perpetual investments
    echo "GICS for perpetual for rothschild/perpetual investments"
    for mailPeople in rbcdexiaperformance@rbcdexia.com austperformance@jpmorgan.com; do 
                ### DEBUG START ###
            if [ $debugMode -eq 1 ]; then
                mailPeople=$ownerMail
            fi
            ### DEBUG END ###
        perpDoDaily300Gics.sh $noSendArg -d `dateformat -i $today` -m $mailPeople 2>&1 | tee $logDir/perpDoDaily300Gics.log | mailx -s "Perpetual/Rothschild GICS 4pm daily (perpDoDaily300Gics.sh)" $ownerMail
    done
}

bt_back_office_load_np_split () {

# (2) BT - Backoffice Data (FMC + Extra) NO PROP SPLIT
    echo "BT - Backoffice Data NO PROP SPLIT"
    btBackOfficeLoad.sh $noSendArg -h -d $today 2>&1 | tee $logDir/$1.log | mailx -s "BT - 4pm Backoffice Data (no prop split)" $ownerMail
    if [ $Dow = "Mon" ]; then
        echo "BT - Backoffice Data [Sunday] (no prop split)"
        btBackOfficeLoad.sh $noSendArg -h -d `dateformat -p $today` 2>&1 | tee $logDir/btBackOfficeLoad1.log | mailx -s "BT - 4pm Backoffice Data (no prop split)" $ownerMail

        echo "BT - Backoffice Data [Saturday] (no prop split)"
        btBackOfficeLoad.sh $noSendArg -h -d `dateformat -pp $today` 2>&1 | tee $logDir/btBackOfficeLoad2.log | mailx -s "BT - 4pm Backoffice Data (no prop split)" $ownerMail
        cat $logDir/btBackOfficeLoad*.log > $logDir/$1.log
    fi
}

cfs_daily_gics() {
# (3) CFS - 4pm Index Data
    echo "CFS - 4pm Index Data"
    cfsdailyGICS.sh -p -d $today 2>&1 | tee $logDir/$1.log | mailx -s "CFS 4pm Data" $ownerMail
}

cfs_data() {

# (4) CFS - Preliminary Market Data
    echo "CFS Market Data (Preliminary)"
    ( cfsData.sh -d $today 2>&1 | tee $logDir/$1.log | mailx -s "CFS -- Market Data (Preliminary)" $ownerMail )
}

cc_data_decommision() {
    # (5) Continuum - Preliminary Market Data
    echo "Continuum Capital Market Data (Preliminary)"
    ( ccData4pm_post_mis_decommission.sh -d $today 2>&1 ) | tee $logDir/$1.log | mailx -s "Continuum -- Market Data (Preliminary)" $ownerMail
}

esuper_fund_eod_prices() {
    # (6)ESuperFund EOD Prices
    ( esuperfundEodPrices.sh 2>&1 ) | tee $logDir/$1.log | mailx -s "EsuperFund eod prices job" $ownerMail
}

atb_data_flag () {


########## After tax data and flags - Begin ###############

    # (7) after tax benchmark - data flags
    echo "After tax benchmark - data flag"
    fn=`read_config.py -s aftertax:data_dir`/flagfiles/${today}.Data.Prelim
    touch $fn
    if [ $? -ne 0 ]; then
        echo ERROR: Could not touch flag file ${fn} | tee $logDir/$1.log | mailx -s "ATB - Data flag" $ownerMail
    else
        echo Set flag: ${fn} | tee $logDir/$1.log | mailx -s "ATB - Data flag" $ownerMail
    fi
}
########## After tax data and flags - End ###############

defect_check () {

# following line should be the last line of this script
# because it checks for potential errors in the output
# produced by all the jobs
    # egrep -i "no|err|fail|ille|unab|segm|warn|trace|core|denied|get_old_prices|timed out|time out|abort|deadlock|Duplicate entries in|can't|don't|must be > 0|test" $logDir/* | egrep -vi 'keyboard|nov|extra|B-FTAU|Translists|DbaseGetCashPosition|cannonical|Fill|Ooh, err,|E-SNO|E-PNO|E-BNO|E-GDNO|E-BNOOA|E-MANO|E-CNNOA|E-MMNO|E-ANO|E-GYNO|E-GPNOA|E-BNOOB|E-TJNO|E-ERNO|E-LINO|E-WTNO|E-HZNO|E-NOD|E-ATNOA|E-PNOOA|E-BWNO|E-BCNO|E-AHNO|E-CLNO|E-PUNO|E-TXNO|E-SNO|E-PNO|E-BNO|E-GDNO|E-BNOOA|E-MANO|E-CNNOA|E-MMNO|E-ANO|E-GYNO|E-GPNOA|E-BNOOB|E-TJNO|E-ERNO|E-LINO|E-WTNO|E-HZNO|E-NOD|E-ATNOA|E-PNOOA|E-BWNO|E-BCNO|E-AHNO|E-CLNO|E-PUNO|E-TXNO|E-XENO|native python|DeprecationWarning|deferredDelivery| = pack\(|No of Trades|Tolhurst|Merrill|Reynolds|No changes|NOD|nonibWeeklyData.py|B-JQUIT|\[additional.txt\]|no data to send' > /tmp/errors.afternoonJobs
    if [ `wc -l /tmp/errors.afternoonJobs | awk '{print $1}'` -ne 0 ]; then
        warn "Potential errors exist in afternoon jobs"
        cat /tmp/errors.afternoonJobs | mailx -s "afternoonJobs: potential errors from log directory" $ownerMail
    fi
}

case $task in
    1) logger 'gics_perpetual' gics_perpetual;;
    2) logger 'bt_back_office_load_np_split' bt_back_office_load_np_split;;
    3) logger 'cfs_daily_gics' cfs_daily_gics;;
    4) logger 'cfs_data' cfs_data;;
    5) logger 'cc_data_decommision' cc_data_decommision;;
    6) logger 'esuper_fund_eod_prices' esuper_fund_eod_prices;;
    7) logger 'atb_data_flag' atb_data_flag;;

    *) echo "Curret task num: $task"; 
esac

if [ $task -eq 0 ]; then
    logger 'gics_perpetual' gics_perpetual
    logger 'bt_back_office_load_np_split' bt_back_office_load_np_split
    logger 'cfs_daily_gics' cfs_daily_gics
    logger 'cfs_data' cfs_data
    logger 'cc_data_decommision' cc_data_decommision
    logger 'esuper_fund_eod_prices' esuper_fund_eod_prices
    logger 'atb_data_flag' atb_data_flag
    # logger 'defect_check' defect_check
fi

defect_check


# got to the end of the script
echo "4pm tasks seem to have been completed" | mail $ownerMail
echo "4pm tasks seem to have been completed"
